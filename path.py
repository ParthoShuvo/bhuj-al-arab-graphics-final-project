import os

__root__ = os.path.dirname(__file__)


def get_image_path():
    img_dir_name = 'images'
    image_path = os.path.join(__root__, img_dir_name)
    return image_path
