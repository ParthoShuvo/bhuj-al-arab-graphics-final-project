class CoordinateMapper(object):
    def __init__(self, original_coord, texture_coord):
        self.__original_coord = original_coord
        self.__texture_coord = texture_coord

    @property
    def original_coord(self):
        return self.__original_coord

    @property
    def texture_coord(self):
        return self.__texture_coord

    def show_map(self):
        text = 'original-coord:' + ','.join(map(str, self.original_coord)) + ',' + ' maps to->' \
               + 'texture-coord:' + ','.join(map(str, self.texture_coord))
        return text
