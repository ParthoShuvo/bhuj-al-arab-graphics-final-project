import os.path
from OpenGL.GL import *
from OpenGL.GLUT import *
from PIL import Image
import numpy
from path import get_image_path


def draw_triangle():
    print('#### triangle is drawing #####')
    # TODO implement triangle function here


def draw_quad(texture_coord_to_original_coord_map, texture, rgba_color, translate_coord, scaling_coord, rotaing_coord):
    # TODO OPTIMIZE this code
    glPushMatrix()
    has_texture = False if not texture else True
    if has_texture:
        glBindTexture(GL_TEXTURE_2D, texture)
        glBegin(GL_QUADS)
        for val in texture_coord_to_original_coord_map.values():
            texture_coord = val.texture_coord
            original_coord = val.original_coord
            glTexCoord2f(texture_coord[0], texture_coord[1])
            glVertex3f(original_coord[0], original_coord[1], original_coord[2])
    else:
        glColor3f(rgba_color[0], rgba_color[1], rgba_color[2])
        glBegin(GL_QUADS)
        for val in texture_coord_to_original_coord_map.values():
            original_coord = val.original_coord
            glVertex3f(original_coord[0], original_coord[1], original_coord[2])
    glEnd()
    glPopMatrix()
    glFlush()


def draw_quad_with_texture(texture_coord_to_original_coord_map, texture, normal_vector=None,
                           translate_coord=None, scaling_coord=None, rotaing_coord=None):
    # TODO add translate coord, scaling coord, rotating coord
    glPushMatrix()
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glBegin(GL_QUADS)
    if normal_vector:
        glNormal3f(normal_vector[0], normal_vector[1], normal_vector[2])
    for val in texture_coord_to_original_coord_map.values():
        texture_coord = val.texture_coord
        original_coord = val.original_coord
        glTexCoord2f(texture_coord[0], texture_coord[1])
        glVertex3f(original_coord[0], original_coord[1], original_coord[2])
    glEnd()
    glPopMatrix()
    glDisable(GL_TEXTURE_2D)



def get_texture(image_file_name):
    image_path = os.path.join(get_image_path(), image_file_name)
    if os.path.isfile(image_path):
        print('##### image file is found #####')
        image = Image.open(image_path).transpose(Image.FLIP_TOP_BOTTOM)
        width = image.size[0]
        height = image.size[1]
        print('size of the image:', width, 'x', height)
        img_data = numpy.array(list(image.getdata()), numpy.int8)
        texture = glGenTextures(1)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glBindTexture(GL_TEXTURE_2D, texture)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        print(texture)
        print('###### texture is loaded #####')
        return texture
