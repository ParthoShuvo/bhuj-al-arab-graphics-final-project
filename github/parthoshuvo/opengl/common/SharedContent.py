class SharedContent(object):
    __day_display_mood = True

    @classmethod
    def get_day_display_mood(cls):
        return cls.__day_display_mood

    @classmethod
    def toggle_day_display_mood(cls):
        cls.__day_display_mood = not cls.__day_display_mood
