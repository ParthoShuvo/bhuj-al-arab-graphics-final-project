import builtins


from github.parthoshuvo.objects.BaseObject import BaseObject
from github.parthoshuvo.opengl.common.Common import *
from github.parthoshuvo.objects import ImagesFactory
from github.parthoshuvo.objects import ObjectsFactory


class WaterObject(BaseObject):



    # __default_original_coordinates = [
    #     [-70.5, -4.0, 33.5],  # A
    #     [70.5, -4.0, 33.5],  # B
    #     [70.5, -4.0, -33.5],  # C
    #     [-70.5, -4.0, -33.5]  # D
    # ]

    __default_original_coordinates = [
        [-70.5, -4.0, 70.5],  # A
        [70.5, -4.0, 70.5],  # B
        [70.5, -4.0, -70.5],  # C
        [-70.5, -4.0, -70.5]  # D
    ]


    __default_texture_coordinates = [
        [0.0, 0.0],
        [1.0, 0.0],
        [1.0, 1.0],
        [0.0, 1.0]
    ]


    __default_normal = [0.0, 1.0, 0.0]

    def __init__(self, original_coord=[], texture_coord=[], normal_vector=[], texture='water.bmp'):
        super(WaterObject, self).__init__()
        self._set_texture_original_coord_map(texture_coord=texture_coord, original_coord=original_coord)
        self.normal_vector = normal_vector
        self.texture = get_texture(texture)
        print("Water object name is", texture)
        print('###### new water object has been created ######')


    def load_default_coords(self):
        pass

    def draw(self):
        self.__set_lightening()
        if self.texture_coordinates and self.texture:
            draw_quad_with_texture(self.texture_coord_original_coord_map, self.texture, self.normal_vector)

    @classmethod
    def get_default_texture_coords(cls):
        return cls.__default_texture_coordinates

    @classmethod
    def get_default_original_coords(cls):
        return cls.__default_original_coordinates

    @classmethod
    def get_default_normal_vector(cls):
        return cls.__default_normal

    @classmethod
    def get_default_texture(cls):
        return 'water.bmp'

    def __set_lightening(self):
        ambient_light = [0.2, 0.2, 0.2, 1.0]
        defused_light = [0.8, 0.8, 0.8, 1.0]
