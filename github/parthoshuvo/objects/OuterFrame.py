from github.parthoshuvo.objects.BaseObject import BaseObject
from OpenGL.GL import *
from OpenGL.GLUT import *
from github.parthoshuvo.store_keeper import StoreKeeper
from github.parthoshuvo.opengl.common.SharedContent import SharedContent



class OuterFrame(BaseObject):
    __default_verticesZ = -2.0
    __default_verticesXY = [
        9.73, 0.71,
        9.8, 1.1,
        9.85, 1.3,
        9.9, 1.6,
        9.94, 1.8,
        10.017, 2.2,
        10.1, 2.8,
        10.14, 3.1,
        10.2, 3.6,
        10.25, 4.0,
        10.29, 4.6,
        10.3, 4.7,
        10.315, 4.8,
        10.336, 5.2,
        10.35, 5.6,
        10.355, 5.7,
        10.36, 6.7,
        10.35, 7.2,
        10.34, 7.3,
        10.315, 7.9,
        10.3, 8.0,
        10.285, 8.1,
        10.25, 8.6,
        10.2, 9.1,
        10.14, 9.5,
        10.1, 9.8,
        10.049, 10.1,
        10, 10.4,
        9.885, 10.9,
        9.8, 11.3,
        9.722, 11.7,
        9.6, 12.1,
        9.54, 12.3,
        9.4, 12.8,
        9.3, 13.1,
        9.2, 13.4,
        9.1, 13.7,
        8.9, 14.2,
        8.7, 14.7,
        8.53, 15.1,
        8.45, 15.3,
        8.4, 15.4,
        8.3, 15.6,
        8.2, 15.8,
        8.105, 16.0,
        8.013, 16.2,
        7.9, 16.4,
        7.85, 16.5,
        7.8, 16.6,
        7.684, 16.8,
        7.575, 17.0,
        7.425, 17.3,
        7.28, 17.5,
        7.12, 17.8,
        6.85, 18.2,
        6.78, 18.3,
        6.664, 18.5,
        6.58, 18.6,
        6.45, 18.8,
        6.31, 19.0,
        6.1, 19.3,
        5.945, 19.5,
        5.8, 19.7,
        5.64, 19.9,
        5.4, 20.2,
        5.15, 20.5,
        4.9, 20.8,
        4.8, 20.9,
        4.63, 21.1,
        4.45, 21.3,
        4.26, 21.5,
        4.06, 21.7,
        3.87, 21.9,
        3.66, 22.1,
        3.46, 22.3,
        3.32, 22.45,
    ]

    __default_curvedBarWidth = 0.5
    __default_curvedBarFrontZ = -2.0
    __default_curvedBarBackZ = -2.25

    def load_default_coords(self):
        pass

    def draw(self):
        self.__draw__left_frame(angle=-80.0, secondary_angle=-35.2, translate_x_factor=-2.5, translate_z_factor=-30.22)
        self.__draw_right_frame(angle=-80.0, secondary_angle=20.2, translate_x_factor=-0.6, translate_z_factor=-26.32)

    def __init__(self):
        super(OuterFrame, self).__init__()
        self.__load_stored_objects()

    def __draw_right_frame(self, angle, secondary_angle, translate_x_factor, translate_z_factor):
        # self.__draw__left_frame(angle, secondary_angle, translate_x_factor, translate_z_factor)

        day_display_mood = SharedContent.get_day_display_mood()
        # print('outer frame :', day_display_mood)

        # translate_factor_x = -33.0
        translate_factor_x = translate_z_factor
        translate_factor_y = -12.0
        # translate_factor_z = -3.5
        translate_factor_z = translate_x_factor
        scale_factor_x = 0.25
        scale_factor_y = 0.2
        scale_factor_z = 0.3
        rotate_y_factor = 1.0
        rotate_x_factor = 0.0
        rotate_z_factor = 0.0
        # angle = -80.0
        # secondary_angle = -30.0
        if day_display_mood:
            glColor3f(0.5, 0.5, 0.5)

        # glDisable(GL_TEXTURE_2D)
        glEnable(GL_MULTISAMPLE)
        # Front Side of the Curved-Bar
        glPushMatrix()
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(1.0, 0.0, 0.0)

        for i in range(len(self.__front_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__front_side_curved_bar[i])):
                glVertex(self.__front_side_curved_bar[i][j][0], self.__front_side_curved_bar[i][j][1],
                         self.__front_side_curved_bar[i][j][2])
            glEnd()

        glPopMatrix()

        # Back Side of the Curved-Bar
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.0, 1.0, 0.0)

        for i in range(len(self.__back_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__back_side_curved_bar[i])):
                glVertex(self.__back_side_curved_bar[i][j][0], self.__back_side_curved_bar[i][j][1],
                         self.__back_side_curved_bar[i][j][2])
            glEnd()

        glPopMatrix()

        # Right Side of the Curved-Bar
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.0, 0.0, 1.0)

        # glBegin(GL_TRIANGLE_FAN)
        for i in range(len(self.__right_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__right_side_curved_bar[i])):
                glVertex(self.__right_side_curved_bar[i][j][0], self.__right_side_curved_bar[i][j][1],
                         self.__right_side_curved_bar[i][j][2])
            glEnd()
        # glEnd()
        glPopMatrix()

        # Left Side of the Curved-Bar
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.5, 0.5, 0.0)

        # glBegin(GL_TRIANGLE_FAN)
        for i in range(len(self.__left_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__left_side_curved_bar[i])):
                glVertex(self.__left_side_curved_bar[i][j][0], self.__left_side_curved_bar[i][j][1],
                         self.__left_side_curved_bar[i][j][2])
            glEnd()
        # glEnd()
        glPopMatrix()

        # Draw the vertical Left_side:
        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.5, 0.5, 0.5)

        # Front face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.0, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(1.0, 0.5, 0.5)

        glBegin(GL_POLYGON)
        glVertex3f(4.013, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(5.4, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarFrontZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        # Right face:

        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.5, 0.5, 1.0)

        glBegin(GL_POLYGON)
        glVertex3f(5.4, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(5.4, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarBackZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        #  Back face:

        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.5, 1.5, 0.5)

        glBegin(GL_POLYGON)
        glVertex3f(4.013, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(5.4, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarBackZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Left face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.013, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(4.013, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarBackZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        glPopMatrix()

        # First Horizontal-Bar:

        # glTranslatef(-8.0, -5.5, -5.0);
        # glRotatef(_angle, 1, 0, 0);
        vertexZ = -2.0
        length_X = 5.66
        length_Y = 6.4 - 6.052
        # Front face:


        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.75, 0.25, 0.25)

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glBegin(GL_POLYGON)
        glVertex3f(4.7, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.69, 6.4, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        # Top face:
        glPushMatrix()
        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)
        # glColor3f(0.25, 0.25, 0.75)
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glBegin(GL_POLYGON)
        glVertex3f(4.69, 6.4, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.69, 6.4, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Back face
        glPushMatrix()

        # glColor3f(0.25, 0.75, 0.25)
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.75, 0.25)

        glBegin(GL_POLYGON)
        glVertex3f(4.7, 6.052, self.__default_curvedBarBackZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarBackZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.69, 6.4, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Bottom face:
        glPushMatrix()

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.7, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarBackZ)
        glVertex3f(4.7, 6.052, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Second Horizontal - Bar:

        vertexZ = -2.0
        length_X = 5.39
        lenght_Y = 11.125 - 10.85

        # Front face:
        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.75, 0.25, 0.25)

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glBegin(GL_POLYGON)
        glVertex3f(4.5, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.5, 11.125, self.__default_curvedBarFrontZ)

        glEnd()
        glPopMatrix()

        # Back face:
        glPushMatrix()

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.75, 0.25)

        glBegin(GL_POLYGON)
        glVertex3f(4.5, 10.85, self.__default_curvedBarBackZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarBackZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.5, 11.125, self.__default_curvedBarBackZ)

        glEnd()
        glPopMatrix()

        # Top face:

        glPushMatrix()

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.5, 11.125, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.5, 11.125, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Bottom face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.5, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarBackZ)
        glVertex3f(4.5, 10.85, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Third Horizontal - Bar:

        vertexZ = -2.0
        length_X = 4.05
        lenght_Y = 15.8 - 15.55

        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        if not day_display_mood:
            glColor3f(0.75, 0.25, 0.25)
        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.3, 15.8, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        # Back face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.75, 0.25)
        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.55, self.__default_curvedBarBackZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarBackZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.3, 15.8, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Top face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.8, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.3, 15.8, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Bottom face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)
        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarBackZ)
        glVertex3f(4.3, 15.55, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        glDisable(GL_MULTISAMPLE)

    def __draw__left_frame(self, angle, secondary_angle, translate_x_factor, translate_z_factor):

        day_display_mood = SharedContent.get_day_display_mood()
        # print('outer frame :', day_display_mood)

        # translate_factor_x = -33.0
        translate_factor_x = translate_z_factor
        translate_factor_y = -12.0
        # translate_factor_z = -3.5
        translate_factor_z = translate_x_factor
        scale_factor_x = 0.23
        scale_factor_y = 0.2
        scale_factor_z = 0.3
        rotate_y_factor = 1.0
        rotate_x_factor = 0.0
        rotate_z_factor = 0.0
        # angle = -80.0
        # secondary_angle = -30.0
        if day_display_mood:
            glColor3f(0.5, 0.5, 0.5)

        # glDisable(GL_TEXTURE_2D)
        glEnable(GL_MULTISAMPLE)
        # Front Side of the Curved-Bar
        glPushMatrix()
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(1.0, 0.0, 0.0)

        for i in range(len(self.__front_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__front_side_curved_bar[i])):
                glVertex(self.__front_side_curved_bar[i][j][0], self.__front_side_curved_bar[i][j][1],
                         self.__front_side_curved_bar[i][j][2])
            glEnd()

        glPopMatrix()

        # Back Side of the Curved-Bar
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.0, 1.0, 0.0)

        for i in range(len(self.__back_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__back_side_curved_bar[i])):
                glVertex(self.__back_side_curved_bar[i][j][0], self.__back_side_curved_bar[i][j][1],
                         self.__back_side_curved_bar[i][j][2])
            glEnd()

        glPopMatrix()

        # Right Side of the Curved-Bar
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.0, 0.0, 1.0)

        # glBegin(GL_TRIANGLE_FAN)
        for i in range(len(self.__right_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__right_side_curved_bar[i])):
                glVertex(self.__right_side_curved_bar[i][j][0], self.__right_side_curved_bar[i][j][1],
                         self.__right_side_curved_bar[i][j][2])
            glEnd()
        # glEnd()
        glPopMatrix()

        # Left Side of the Curved-Bar
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.5, 0.5, 0.0)

        # glBegin(GL_TRIANGLE_FAN)
        for i in range(len(self.__left_side_curved_bar)):
            glBegin(GL_TRIANGLE_FAN)
            for j in range(len(self.__left_side_curved_bar[i])):
                glVertex(self.__left_side_curved_bar[i][j][0], self.__left_side_curved_bar[i][j][1],
                         self.__left_side_curved_bar[i][j][2])
            glEnd()
        # glEnd()
        glPopMatrix()

        # Draw the vertical Left_side:
        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.5, 0.5, 0.5)

        # Front face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.0, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(1.0, 0.5, 0.5)

        glBegin(GL_POLYGON)
        glVertex3f(4.013, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(5.4, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarFrontZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        # Right face:

        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.5, 0.5, 1.0)

        glBegin(GL_POLYGON)
        glVertex3f(5.4, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(5.4, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(4.85, 2.384, self.__default_curvedBarFrontZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarBackZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        #  Back face:

        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.5, 1.5, 0.5)

        glBegin(GL_POLYGON)
        glVertex3f(4.013, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(5.4, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glVertex3f(4.85, 2.384, self.__default_curvedBarBackZ)
        glVertex3f(4.105, 21.66, self.__default_curvedBarBackZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Left face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.013, 0.7, self.__default_curvedBarFrontZ)
        glVertex3f(4.013, 0.7, self.__default_curvedBarBackZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glEnd()

        glBegin(GL_POLYGON)
        glVertex3f(3.17, 3.55, self.__default_curvedBarFrontZ)
        glVertex3f(3.17, 3.55, self.__default_curvedBarBackZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarBackZ)
        glVertex3f(3.32, 22.45, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        glPopMatrix()

        # First Horizontal-Bar:

        # glTranslatef(-8.0, -5.5, -5.0);
        # glRotatef(_angle, 1, 0, 0);
        vertexZ = -2.0
        length_X = 5.66
        length_Y = 6.4 - 6.052
        # Front face:


        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.75, 0.25, 0.25)

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glBegin(GL_POLYGON)
        glVertex3f(4.7, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.69, 6.4, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        # Top face:
        glPushMatrix()
        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)
        # glColor3f(0.25, 0.25, 0.75)
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glBegin(GL_POLYGON)
        glVertex3f(4.69, 6.4, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.69, 6.4, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Back face
        glPushMatrix()

        # glColor3f(0.25, 0.75, 0.25)
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.75, 0.25)

        glBegin(GL_POLYGON)
        glVertex3f(4.7, 6.052, self.__default_curvedBarBackZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarBackZ)
        glVertex3f(4.7 + length_X, 6.052 + length_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.69, 6.4, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Bottom face:
        glPushMatrix()

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.7, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarFrontZ)
        glVertex3f(4.7 + length_X, 6.052, self.__default_curvedBarBackZ)
        glVertex3f(4.7, 6.052, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Second Horizontal - Bar:

        vertexZ = -2.0
        length_X = 5.39
        lenght_Y = 11.125 - 10.85

        # Front face:
        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.75, 0.25, 0.25)

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glBegin(GL_POLYGON)
        glVertex3f(4.5, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.5, 11.125, self.__default_curvedBarFrontZ)

        glEnd()
        glPopMatrix()

        # Back face:
        glPushMatrix()

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.75, 0.25)

        glBegin(GL_POLYGON)
        glVertex3f(4.5, 10.85, self.__default_curvedBarBackZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarBackZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.5, 11.125, self.__default_curvedBarBackZ)

        glEnd()
        glPopMatrix()

        # Top face:

        glPushMatrix()

        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.5, 11.125, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.5, 11.125, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Bottom face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.5, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarFrontZ)
        glVertex3f(4.5 + length_X, 10.85, self.__default_curvedBarBackZ)
        glVertex3f(4.5, 10.85, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Third Horizontal - Bar:

        vertexZ = -2.0
        length_X = 4.05
        lenght_Y = 15.8 - 15.55

        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        if not day_display_mood:
            glColor3f(0.75, 0.25, 0.25)
        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.3, 15.8, self.__default_curvedBarFrontZ)
        glEnd()
        glPopMatrix()

        # Back face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.25, 0.75, 0.25)
        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.55, self.__default_curvedBarBackZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarBackZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.3, 15.8, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Top face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)

        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.8, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55 + lenght_Y, self.__default_curvedBarBackZ)
        glVertex3f(4.3, 15.8, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        # Bottom face:
        glPushMatrix()
        # glColor3f(0.5, 0.5, 0.5)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
        glTranslatef(translate_factor_x, -12.2, translate_factor_z)
        glRotatef(secondary_angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if not day_display_mood:
            glColor3f(0.75, 0.75, 0.75)
        glBegin(GL_POLYGON)
        glVertex3f(4.3, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarFrontZ)
        glVertex3f(4.3 + length_X, 15.55, self.__default_curvedBarBackZ)
        glVertex3f(4.3, 15.55, self.__default_curvedBarBackZ)
        glEnd()
        glPopMatrix()

        glDisable(GL_MULTISAMPLE)

    def __load_stored_objects(self):
        self.__outer_frame_objects = StoreKeeper.open_object('outer_frame_objects')
        self.__front_side_curved_bar = self.__outer_frame_objects['front_side_curved_bar']
        self.__back_side_curved_bar = self.__outer_frame_objects['back_side_curved_bar']
        self.__right_side_curved_bar = self.__outer_frame_objects['right_side_curved_bar']
        self.__left_side_curved_bar = self.__outer_frame_objects['left_side_curved_bar']


