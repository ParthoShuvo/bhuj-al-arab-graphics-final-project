from abc import ABCMeta, abstractmethod
from collections import OrderedDict
from github.parthoshuvo.coordinate_mapper.CoordinateMapper import CoordinateMapper


class BaseObject(metaclass=ABCMeta):
    def __init__(self):
        self.__original_coordinates = []
        self.__texture_coordinates = []
        self.__texture_coord_original_coord_map = OrderedDict()
        self.__scaling_coord = []
        self.__translating_coord = []
        self.__rotating_coord = []
        self.__normal_vector = []
        self.__rgba_color = [1.0, 1.0, 1.0, 0.0]
        self.__texture = None

    @property
    def normal_vector(self):
        return self.__normal_vector

    @normal_vector.setter
    def normal_vector(self, normal_vector):
        self.__normal_vector = normal_vector

    @property
    def texture(self):
        return self.__texture

    @texture.setter
    def texture(self, texture):
        self.__texture = texture

    @property
    def original_coordinates(self):
        return self.__original_coordinates

    @original_coordinates.setter
    def original_coordinates(self, original_coordinates):
        self.__original_coordinates = original_coordinates

    @property
    def texture_coordinates(self):
        return self.__texture_coordinates

    @texture_coordinates.setter
    def texture_coordinates(self, texture_coordinates):
        self.__texture_coordinates = texture_coordinates

    @property
    def texture_coord_original_coord_map(self):
        return self.__texture_coord_original_coord_map

    def set_texture_coord_original_coord_map(self, texture_coord, original_coord):
        if texture_coord and original_coord:
            print('#### new map set ######')
            for i in range(0, len(self.__texture_coordinates)):
                self.__texture_coord_original_coord_map[i + 1] = CoordinateMapper(
                    original_coord=original_coord[i], texture_coord=texture_coord[i])

    @property
    def rgba_color(self):
        return self.__rgba_color

    @rgba_color.setter
    def rgba_color(self, rgba_color):
        self.__rgba_color = rgba_color

    @property
    def scaling_coord(self):
        return self.__scaling_coord

    @scaling_coord.setter
    def scaling_coord(self, scaling_coord):
        self.__scaling_coord = scaling_coord

    @property
    def translating_coord(self):
        return self.__translating_coord

    @translating_coord.setter
    def translating_coord(self, translating_coord):
        self.__translating_coord = translating_coord

    @property
    def rotating_coord(self):
        return self.__rotating_coord

    @rotating_coord.setter
    def rotating_coord(self, rotating_coord):
        self.__rotating_coord = rotating_coord

    @abstractmethod
    def draw(self):
        ...

    @abstractmethod
    def load_default_coords(self):
        ...

    def test_object_member_value(self):
        if self.texture_coord_original_coord_map:
            for key, val in self.texture_coord_original_coord_map.items():
                msg = 'point-coord:' + str(key) + '# ' + val.show_map()
                print(msg)

    def _set_texture_original_coord_map(self, texture_coord, original_coord):
        self.texture_coordinates = texture_coord
        self.original_coordinates = original_coord
        self.set_texture_coord_original_coord_map(
            texture_coord=self.texture_coordinates, original_coord=self.original_coordinates)
