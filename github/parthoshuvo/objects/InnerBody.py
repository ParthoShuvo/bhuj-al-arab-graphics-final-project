from github.parthoshuvo.objects.BaseObject import BaseObject
from github.parthoshuvo.store_keeper import StoreKeeper
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from github.parthoshuvo.opengl.common.Common import *


class InnerBody(BaseObject):
    # Drawing the Right - Face:
    __scaled_vertices_XY = [
        9.535400, 0.710000,
        9.604000, 1.100000,
        9.653000, 1.300000,
        9.702000, 1.600000,
        9.741200, 1.800000,
        9.816660, 2.200000,
        9.898000, 2.800000,
        9.937200, 3.100000,
        9.996000, 3.600000,
        10.045000, 4.000000,
        10.084200, 4.600000,
        10.094000, 4.700000,
        10.108700, 4.800000,
        10.129280, 5.200000,
        10.143000, 5.600000,
        10.147900, 5.700000,
        10.152800, 6.700000,
        10.143000, 7.200000,
        10.133200, 7.300000,
        10.108700, 7.900000,
        10.094000, 8.000000,
        10.079300, 8.100000,
        10.045000, 8.600000,
        9.996000, 9.100000,
        9.937200, 9.500000,
        9.898000, 9.800000,
        9.848020, 10.100000,
        9.800000, 10.400000,
        9.687300, 10.900000,
        9.604000, 11.300000,
        9.527560, 11.700000,
        9.408000, 12.100000,
        9.349200, 12.300000,
        9.212000, 12.800000,
        9.114000, 13.100000,
        9.016000, 13.400000,
        8.918000, 13.700000,
        8.722000, 14.200000,
        8.526000, 14.700000,
        8.359400, 15.100000,
        8.281000, 15.300000,
        8.232000, 15.400000,
        8.134000, 15.600000,
        8.036000, 15.800000,
        7.942900, 16.000000,
        7.852740, 16.200000,
        7.742000, 16.400000,
        7.693000, 16.500000,
        7.644000, 16.600000,
        7.530320, 16.800000,
        7.423500, 17.000000,
    ]

    # Drawing the Left - face:
    __scaled_vertices_XYZ = [
        2.625000, 0.710000, 5.214300,
        2.625000, 1.100000, 5.278000,
        2.625000, 1.300000, 5.323500,
        2.625000, 1.600000, 5.369000,
        2.625000, 1.800000, 5.405400,
        2.625000, 2.200000, 5.475470,
        2.625000, 2.800000, 5.551000,
        2.625000, 3.100000, 5.587400,
        2.625000, 3.600000, 5.642000,
        2.625000, 4.000000, 5.687500,
        2.625000, 4.600000, 5.723900,
        2.625000, 4.700000, 5.733000,
        2.625000, 4.800000, 5.746650,
        2.625000, 5.200000, 5.765760,
        2.625000, 5.600000, 5.778500,
        2.625000, 5.700000, 5.783050,
        2.625000, 6.700000, 5.787600,
        2.625000, 7.200000, 5.778500,
        2.625000, 7.300000, 5.769400,
        2.625000, 7.900000, 5.746650,
        2.625000, 8.000000, 5.733000,
        2.625000, 8.100000, 5.719350,
        2.625000, 8.600000, 5.687500,
        2.625000, 9.100000, 5.642000,
        2.625000, 9.500000, 5.587400,
        2.625000, 9.800000, 5.551000,
        2.625000, 10.100000, 5.504590,
        2.625000, 10.400000, 5.460000,
        2.625000, 10.900000, 5.355350,
        2.625000, 11.300000, 5.278000,
        2.625000, 11.700000, 5.207020,
        2.625000, 12.100000, 5.096000,
        2.625000, 12.300000, 5.041400,
        2.625000, 12.800000, 4.914000,
        2.625000, 13.100000, 4.823000,
        2.625000, 13.400000, 4.732000,
        2.625000, 13.700000, 4.641000,
        2.625000, 14.200000, 4.459000,
        2.625000, 14.700000, 4.277000,
        2.625000, 15.100000, 4.122300,
        2.625000, 15.300000, 4.049500,
        2.625000, 15.400000, 4.004000,
        2.625000, 15.600000, 3.913000,
        2.625000, 15.800000, 3.822000,
        2.625000, 16.000000, 3.735550,
        2.625000, 16.200000, 3.651830,
        2.625000, 16.400000, 3.549000,
        2.625000, 16.500000, 3.503500,
        2.625000, 16.600000, 3.458000,
        2.625000, 16.800000, 3.352440,
        2.625000, 17.000000, 3.253250,
    ]

    # Drawing front curved face:
    __rt_vertices_XY = [
        9.535400, 0.710000,
        9.604000, 1.100000,
        9.653000, 1.300000,
        9.702000, 1.600000,
        9.741200, 1.800000,
        9.816660, 2.200000,
        9.898000, 2.800000,
        9.937200, 3.100000,
        9.996000, 3.600000,
        10.045000, 4.000000,
        10.084200, 4.600000,
        10.094000, 4.700000,
        10.108700, 4.800000,
        10.129280, 5.200000,
        10.143000, 5.600000,
        10.147900, 5.700000,
        10.152800, 6.700000,
        10.143000, 7.200000,
        10.133200, 7.300000,
        10.108700, 7.900000,
        10.094000, 8.000000,
        10.079300, 8.100000,
        10.045000, 8.600000,
        9.996000, 9.100000,
        9.937200, 9.500000,
        9.898000, 9.800000,
        9.848020, 10.100000,
        9.800000, 10.400000,
        9.687300, 10.900000,
        9.604000, 11.300000,
        9.527560, 11.700000,
        9.408000, 12.100000,
        9.349200, 12.300000,
        9.212000, 12.800000,
        9.114000, 13.100000,
        9.016000, 13.400000,
        8.918000, 13.700000,
        8.722000, 14.200000,
        8.526000, 14.700000,
        8.359400, 15.100000,
        8.281000, 15.300000,
        8.232000, 15.400000,
        8.134000, 15.600000,
        8.036000, 15.800000,
        7.942900, 16.000000,
        7.852740, 16.200000,
        7.742000, 16.400000,
        7.693000, 16.500000,
        7.644000, 16.600000,
        7.530320, 16.800000,
        7.423500, 17.000000,
    ]

    __lf_vertices_XYZ = [
        2.625000, 0.710000, 5.214300,
        2.625000, 1.100000, 5.278000,
        2.625000, 1.300000, 5.323500,
        2.625000, 1.600000, 5.369000,
        2.625000, 1.800000, 5.405400,
        2.625000, 2.200000, 5.475470,
        2.625000, 2.800000, 5.551000,
        2.625000, 3.100000, 5.587400,
        2.625000, 3.600000, 5.642000,
        2.625000, 4.000000, 5.687500,
        2.625000, 4.600000, 5.723900,
        2.625000, 4.700000, 5.733000,
        2.625000, 4.800000, 5.746650,
        2.625000, 5.200000, 5.765760,
        2.625000, 5.600000, 5.778500,
        2.625000, 5.700000, 5.783050,
        2.625000, 6.700000, 5.787600,
        2.625000, 7.200000, 5.778500,
        2.625000, 7.300000, 5.769400,
        2.625000, 7.900000, 5.746650,
        2.625000, 8.000000, 5.733000,
        2.625000, 8.100000, 5.719350,
        2.625000, 8.600000, 5.687500,
        2.625000, 9.100000, 5.642000,
        2.625000, 9.500000, 5.587400,
        2.625000, 9.800000, 5.551000,
        2.625000, 10.100000, 5.504590,
        2.625000, 10.400000, 5.460000,
        2.625000, 10.900000, 5.355350,
        2.625000, 11.300000, 5.278000,
        2.625000, 11.700000, 5.207020,
        2.625000, 12.100000, 5.096000,
        2.625000, 12.300000, 5.041400,
        2.625000, 12.800000, 4.914000,
        2.625000, 13.100000, 4.823000,
        2.625000, 13.400000, 4.732000,
        2.625000, 13.700000, 4.641000,
        2.625000, 14.200000, 4.459000,
        2.625000, 14.700000, 4.277000,
        2.625000, 15.100000, 4.122300,
        2.625000, 15.300000, 4.049500,
        2.625000, 15.400000, 4.004000,
        2.625000, 15.600000, 3.913000,
        2.625000, 15.800000, 3.822000,
        2.625000, 16.000000, 3.735550,
        2.625000, 16.200000, 3.651830,
        2.625000, 16.400000, 3.549000,
        2.625000, 16.500000, 3.503500,
        2.625000, 16.600000, 3.458000,
        2.625000, 16.800000, 3.352440,
        2.625000, 17.000000, 3.253250,
    ]

    def __init__(self):
        super(InnerBody, self).__init__()
        self.__load_images()
        self.__load_stored_objects()

    def load_default_coords(self):
        pass

    def draw(self):
        angle = -45.0

        translate_x_factor = -3.6
        translate_y_factor = -12.25
        translate_z_factor = -18.8

        scale_x_factor = 0.2
        scale_y_factor = 0.2
        scale_z_factor = 0.35

        rotate_x_factor = 0.0
        rotate_y_factor = 1.0
        rotate_z_factor = 0.0

        otherTwoVerticesZ = -1.275
        curvedVertices_Z = -1.575

        #  Drawing the Right-Face:
        glPushMatrix()

        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glTranslatef(translate_x_factor, translate_y_factor, translate_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        glEnable(GL_TEXTURE_2D)

        glColor3f(0.5, 0.25, 0.0)
        glBindTexture(GL_TEXTURE_2D, self.__right_curved_portion_img)
        glBegin(GL_POLYGON)
        glTexCoord2f(1.0, 0.0)
        glVertex3f(3.17, 0.7, otherTwoVerticesZ)
        tex_y = 0.0
        for i in range(0, len(self.__scaled_vertices_XY), 2):
            glTexCoord2f(0.0, tex_y)
            glVertex3f(self.__scaled_vertices_XY[i], self.__scaled_vertices_XY[i + 1], curvedVertices_Z)
            tex_y += 0.02
        glTexCoord2f(1.0, 1.0)
        glVertex3f(3.32, 16.7, otherTwoVerticesZ)
        glEnd()
        glDisable(GL_TEXTURE_2D)
        glPopMatrix()

        #  Drawing the Left-face:
        glPushMatrix()

        glEnable(GL_TEXTURE_2D)

        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glTranslatef(translate_x_factor, translate_y_factor, translate_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glColor3f(0.5, 0.25, 0.25)
        glBindTexture(GL_TEXTURE_2D, self.__left_curved_portion_img)
        glBegin(GL_POLYGON)
        glTexCoord(0.0, 0.0)
        glVertex3f(2.944, 0.700000, -0.8134)
        tex_y = 0.0
        for i in range(0, len(self.__scaled_vertices_XYZ), 3):
            glTexCoord(1.0, tex_y)
            glVertex3f(self.__scaled_vertices_XYZ[i], self.__scaled_vertices_XYZ[i + 1],
                       self.__scaled_vertices_XYZ[i + 2])
            tex_y += 0.02

        glTexCoord(0.0, 1.0)
        glVertex3f(2.944, 16.7000, -0.686)
        glEnd()
        glDisable(GL_TEXTURE_2D)
        glPopMatrix()

        # Drawing front curved face:

        rtVertices_Z = -1.575

        glPushMatrix()
        glEnable(GL_TEXTURE_2D)

        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glTranslatef(translate_x_factor, translate_y_factor, translate_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        i = 0
        j = 0
        while i < (51 * 3 - 3) and j < (51 * 2 - 2):
            glBindTexture(GL_TEXTURE_2D, self.__curved_portion_img)
            glBegin(GL_POLYGON)
            glTexCoord(0.0, 0.0)
            glVertex3f(self.__lf_vertices_XYZ[i], self.__lf_vertices_XYZ[i + 1], self.__lf_vertices_XYZ[i + 2])
            glTexCoord(1.0, 0.0)
            glVertex3f(self.__rt_vertices_XY[j], self.__rt_vertices_XY[j + 1], rtVertices_Z)
            glTexCoord(1.0, 1.0)
            glVertex3f(self.__rt_vertices_XY[j + 2], self.__rt_vertices_XY[j + 3], rtVertices_Z)
            glTexCoord(0.0, 1.0)
            glVertex3f(self.__lf_vertices_XYZ[i + 3], self.__lf_vertices_XYZ[i + 4], self.__lf_vertices_XYZ[i + 5])
            glEnd()
            i += 3
            j += 2

        glDisable(GL_TEXTURE_2D)
        glPopMatrix()

        # Drawing the back-face:
        glPushMatrix()

        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glTranslatef(translate_x_factor, translate_y_factor, translate_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glColor3f(0.1, 0.6, 0.1)
        glBegin(GL_POLYGON)
        glVertex3f(3.32, 0.7, -1.275)
        glVertex3f(2.944, 0.700000, -0.8134)
        glVertex3f(2.944, 16.7000, -0.686)
        glVertex3f(3.32, 16.7, -1.275)
        glEnd()
        glPopMatrix()
        #
        #
        # Drawing the roof:
        glPushMatrix()
        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glTranslatef(translate_x_factor, translate_y_factor, translate_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
        glColor3f(0.000, 0.000, 0.502)
        glBegin(GL_POLYGON)
        glVertex3f(7.423500, 17.000000, -1.575)
        glVertex3f(3.32, 16.7, -1.275)
        glVertex3f(2.625000, 17.000000, 3.253250)
        glVertex3f(2.944, 16.7000, -0.686)
        glEnd()
        glPopMatrix()

    def __load_stored_objects(self):
        pass

    def __load_images(self):
        self.__curved_portion_img = get_texture('b.bmp')
        self.__left_curved_portion_img = get_texture('b.bmp')
        self.__right_curved_portion_img = get_texture('b.bmp')
