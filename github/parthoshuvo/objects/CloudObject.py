from github.parthoshuvo.objects.WaterObject import WaterObject

class CloudObject(WaterObject):


    # __default_original_coordinates = [
    #     [-30.5, -4.0, -20.5],  # A
    #     [30.5, -4.0, -20.5],  # B
    #     [30.5, 14.0, -20.5],  # C
    #     [-30.5, 14.0, -20.5]  # D
    # ]

    __default_original_coordinates = [
        # right
        [70.5, -4.0, 70.5],  # A
        [70.5, -4.0, -70.5],  # B
        [70.5, 74.0, -70.5],  # C
        [70.5, 74.0, 70.5],  # D

        # back
        [-70.5, -4.0, -70.5],  # A
        [70.5, -4.0, -70.5],  # B
        [70.5, 74.0, -70.5],  # C
        [-70.5, 74.0, -70.5],  # D

        # left
        [-70.5, -4.0, 70.5],  # A
        [-70.5, -4.0, -70.5],  # B
        [-70.5, 74.0, -70.5],  # C
        [-70.5, 74.0, 70.5],  # D

        #front
        [-70.5, -4.0, 70.5],  # A
        [70.5, -4.0, 70.5],  # B
        [70.5, 74.0, 70.5],  # C
        [-70.5, 74.0, 70.5],  # D

        #top
        [-70.5, 74.0, 70.5],  # A
        [70.5, 74.0, 70.5],  # B
        [70.5, 74.0, 70.5],  # C
        [-70.5, 74.0, 70.5],  # D


    ]



    __default_texture_coordinates = [
        # right
        [0.0, 0.0],
        [1.0, 0.0],
        [1.0, 1.0],
        [0.0, 1.0],

        # back
        [0.0, 0.0],
        [1.0, 0.0],
        [1.0, 1.0],
        [0.0, 1.0],

        # left
        [0.0, 0.0],
        [1.0, 0.0],
        [1.0, 1.0],
        [0.0, 1.0],

        # front
        [0.0, 0.0],
        [1.0, 0.0],
        [1.0, 1.0],
        [0.0, 1.0],

        #top
        [0.0, 0.0],
        [1.0, 0.0],
        [1.0, 1.0],
        [0.0, 1.0],

    ]

    __default_normal = [0.0, 1.0, 0.0]

    def __init__(self, original_coord=[], texture_coord=[], normal_vector=[], texture='cloud.bmp'):
        print(original_coord)
        super(CloudObject, self).__init__(original_coord=original_coord, texture_coord=texture_coord,
                                          normal_vector=normal_vector, texture=texture)



    @classmethod
    def get_default_texture_coords(cls):
        return cls.__default_texture_coordinates

    @classmethod
    def get_default_original_coords(cls):
        return cls.__default_original_coordinates

    @classmethod
    def get_default_normal_vector(cls):
        return cls.__default_normal

    @classmethod
    def get_default_texture(cls):
        return 'cloud.bmp'