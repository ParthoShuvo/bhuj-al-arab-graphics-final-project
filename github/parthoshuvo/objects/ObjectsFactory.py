from github.parthoshuvo.objects.WaterObject import WaterObject
from github.parthoshuvo.objects.TriangleShapeIsland import TriangleShapeIsland

__WATER_OBJECT__ = 'WATER'
__TRIANGLE_ISLAND_OBJECT__ = 'TRIANGLE_ISLAND_OBJECT'


def get_object(object_type):
    display_object = None
    print('##### new object: ' + object_type + ' #######')
    if __WATER_OBJECT__ == object_type:
        display_object = WaterObject()
    elif __TRIANGLE_ISLAND_OBJECT__ == object_type:
        display_object = TriangleShapeIsland()
    return display_object
