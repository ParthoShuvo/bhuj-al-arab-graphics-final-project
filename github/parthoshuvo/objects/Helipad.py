from github.parthoshuvo.objects.BaseObject import BaseObject
from github.parthoshuvo.store_keeper import StoreKeeper
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from github.parthoshuvo.opengl.common.Common import *
from math import *
from github.parthoshuvo.opengl.common.SharedContent import SharedContent


class Helipad(BaseObject):
    __default_vertices_XY = [
        1.000000, 0.000000,
        0.999847, 0.017501,
        0.999387, 0.034997,
        0.998622, 0.052482,
        0.997550, 0.069950,
        0.996173, 0.087398,
        0.994491, 0.104819,
        0.992505, 0.122207,
        0.990214, 0.139558,
        0.987620, 0.156867,
        0.984723, 0.174127,
        0.981525, 0.191334,
        0.978026, 0.208482,
        0.974228, 0.225567,
        0.970131, 0.242582,
        0.965737, 0.259523,
        0.961047, 0.276385,
        0.956063, 0.293162,
        0.950786, 0.309849,
        0.945217, 0.326441,
        0.939360, 0.342934,
        0.933214, 0.359321,
        0.926783, 0.375598,
        0.920067, 0.391760,
        0.913070, 0.407802,
        0.905793, 0.423719,
        0.898239, 0.439507,
        0.890410, 0.455160,
        0.882308, 0.470673,
        0.873935, 0.486042,
        0.865295, 0.501263,
        0.856390, 0.516329,
        0.847223, 0.531238,
        0.837796, 0.545984,
        0.828112, 0.560563,
        0.818175, 0.574969,
        0.807987, 0.589200,
        0.797552, 0.603251,
        0.786872, 0.617116,
        0.775951, 0.630793,
        0.764793, 0.644276,
        0.753400, 0.657562,
        0.741777, 0.670647,
        0.729926, 0.683526,
        0.717852, 0.696196,
        0.705558, 0.708652,
        0.693048, 0.720892,
        0.680325, 0.732910,
        0.667395, 0.744704,
        0.654259, 0.756270,
        0.640924, 0.767605,
        0.627392, 0.778704,
        0.613667, 0.789565,
        0.599755, 0.800184,
        0.585659, 0.810557,
        0.571384, 0.820683,
        0.556934, 0.830557,
        0.542313, 0.840177,
        0.527526, 0.849539,
        0.512577, 0.858641,
        0.497472, 0.867480,
        0.482214, 0.876054,
        0.466808, 0.884359,
        0.451259, 0.892393,
        0.435572, 0.900154,
        0.419752, 0.907639,
        0.403803, 0.914846,
        0.387731, 0.921773,
        0.371539, 0.928417,
        0.355234, 0.934777,
        0.338820, 0.940851,
        0.322303, 0.946637,
        0.305686, 0.952132,
        0.288976, 0.957336,
        0.272177, 0.962247,
        0.255295, 0.966863,
        0.238335, 0.971183,
        0.221302, 0.975205,
        0.204201, 0.978929,
        0.187037, 0.982353,
        0.169817, 0.985476,
        0.152544, 0.988297,
        0.135224, 0.990815,
        0.117863, 0.993030,
        0.100466, 0.994940,
        0.083038, 0.996546,
        0.065585, 0.997847,
        0.048112, 0.998842,
        0.030624, 0.999531,
        0.013126, 0.999914,
        -0.004375, 0.999990,
        -0.021876, 0.999761,
        -0.039369, 0.999225,
        -0.056851, 0.998383,
        -0.074315, 0.997235,
        -0.091756, 0.995782,
        -0.109169, 0.994023,
        -0.126549, 0.991960,
        -0.143890, 0.989594,
        -0.161186, 0.986924,
        -0.178434, 0.983952,
        -0.195627, 0.980678,
        -0.212760, 0.977105,
        -0.229827, 0.973231,
        -0.246825, 0.969060,
        -0.263746, 0.964592,
        -0.280587, 0.959828,
        -0.297342, 0.954771,
        -0.314006, 0.949421,
        -0.330574, 0.943780,
        -0.347041, 0.937850,
        -0.363401, 0.931633,
        -0.379650, 0.925130,
        -0.395782, 0.918344,
        -0.411793, 0.911277,
        -0.427679, 0.903931,
        -0.443433, 0.896308,
        -0.459051, 0.888410,
        -0.474529, 0.880240,
        -0.489861, 0.871800,
        -0.505044, 0.863094,
        -0.520071, 0.854123,
        -0.534940, 0.844890,
        -0.549644, 0.835399,
        -0.564181, 0.825651,
        -0.578544, 0.815651,
        -0.592730, 0.805401,
        -0.606735, 0.794905,
        -0.620553, 0.784164,
        -0.634182, 0.773184,
        -0.647616, 0.761967,
        -0.660852, 0.750516,
        -0.673886, 0.738835,
        -0.686713, 0.726929,
        -0.699330, 0.714799,
        -0.711732, 0.702451,
        -0.723917, 0.689887,
        -0.735880, 0.677112,
        -0.747617, 0.664130,
        -0.759126, 0.650944,
        -0.770402, 0.637559,
        -0.781442, 0.623978,
        -0.792242, 0.610207,
        -0.802800, 0.596248,
        -0.813112, 0.582107,
        -0.823175, 0.567788,
        -0.832986, 0.553294,
        -0.842541, 0.538632,
        -0.851839, 0.523804,
        -0.860876, 0.508815,
        -0.869649, 0.493671,
        -0.878155, 0.478376,
        -0.886393, 0.462934,
        -0.894359, 0.447350,
        -0.902051, 0.431630,
        -0.909467, 0.415777,
        -0.916604, 0.399797,
        -0.923460, 0.383694,
        -0.930034, 0.367474,
        -0.936323, 0.351141,
        -0.942325, 0.334700,
        -0.948038, 0.318157,
        -0.953461, 0.301517,
        -0.958592, 0.284784,
        -0.963429, 0.267964,
        -0.967971, 0.251062,
        -0.972216, 0.234084,
        -0.976164, 0.217033,
        -0.979813, 0.199916,
        -0.983162, 0.182737,
        -0.986209, 0.165503,
        -0.988955, 0.148218,
        -0.991397, 0.130888,
        -0.993536, 0.113517,
        -0.995371, 0.096112,
        -0.996900, 0.078677,
        -0.998124, 0.061218,
        -0.999043, 0.043741,
        -0.999655, 0.026250,
        -0.999962, 0.008751,
        -0.999962, -0.008751,
        -0.999655, -0.026250,
        -0.999043, -0.043741,
        -0.998124, -0.061218,
        -0.996900, -0.078677,
        -0.995371, -0.096112,
        -0.993536, -0.113517,
        -0.991397, -0.130888,
        -0.988955, -0.148218,
        -0.986209, -0.165503,
        -0.983162, -0.182737,
        -0.979813, -0.199916,
        -0.976164, -0.217033,
        -0.972216, -0.234084,
        -0.967971, -0.251062,
        -0.963429, -0.267964,
        -0.958592, -0.284784,
        -0.953461, -0.301517,
        -0.948038, -0.318157,
        -0.942325, -0.334700,
        -0.936323, -0.351141,
        -0.930034, -0.367474,
        -0.923460, -0.383694,
        -0.916604, -0.399797,
        -0.909467, -0.415777,
        -0.902051, -0.431630,
        -0.894359, -0.447350,
        -0.886393, -0.462934,
        -0.878155, -0.478376,
        -0.869649, -0.493671,
        -0.860876, -0.508815,
        -0.851839, -0.523804,
        -0.842541, -0.538632,
        -0.832986, -0.553294,
        -0.823175, -0.567788,
        -0.813112, -0.582107,
        -0.802800, -0.596248,
        -0.792242, -0.610207,
        -0.781442, -0.623978,
        -0.770402, -0.637559,
        -0.759126, -0.650944,
        -0.747617, -0.664130,
        -0.735880, -0.677112,
        -0.723917, -0.689887,
        -0.711732, -0.702451,
        -0.699330, -0.714799,
        -0.686713, -0.726929,
        -0.673886, -0.738835,
        -0.660852, -0.750516,
        -0.647616, -0.761967,
        -0.634182, -0.773184,
        -0.620553, -0.784164,
        -0.606735, -0.794905,
        -0.592730, -0.805401,
        -0.578544, -0.815651,
        -0.564181, -0.825651,
        -0.549644, -0.835399,
        -0.534940, -0.844890,
        -0.520071, -0.854123,
        -0.505044, -0.863094,
        -0.489861, -0.871800,
        -0.474529, -0.880240,
        -0.459051, -0.888410,
        -0.443433, -0.896308,
        -0.427679, -0.903931,
        -0.411793, -0.911277,
        -0.395782, -0.918344,
        -0.379650, -0.925130,
        -0.363401, -0.931633,
        -0.347041, -0.937850,
        -0.330574, -0.943780,
        -0.314006, -0.949421,
        -0.297342, -0.954771,
        -0.280587, -0.959828,
        -0.263746, -0.964592,
        -0.246825, -0.969060,
        -0.229827, -0.973231,
        -0.212760, -0.977105,
        -0.195627, -0.980678,
        -0.178434, -0.983952,
        -0.161186, -0.986924,
        -0.143890, -0.989594,
        -0.126549, -0.991960,
        -0.109169, -0.994023,
        -0.091756, -0.995782,
        -0.074315, -0.997235,
        -0.056851, -0.998383,
        -0.039369, -0.999225,
        -0.021876, -0.999761,
        -0.004375, -0.999990,
        0.013126, -0.999914,
        0.030624, -0.999531,
        0.048112, -0.998842,
        0.065585, -0.997847,
        0.083038, -0.996546,
        0.100466, -0.994940,
        0.117863, -0.993030,
        0.135224, -0.990815,
        0.152544, -0.988297,
        0.169817, -0.985476,
        0.187037, -0.982353,
        0.204201, -0.978929,
        0.221302, -0.975205,
        0.238335, -0.971183,
        0.255295, -0.966863,
        0.272177, -0.962247,
        0.288976, -0.957336,
        0.305686, -0.952132,
        0.322303, -0.946637,
        0.338820, -0.940851,
        0.355234, -0.934777,
        0.371539, -0.928417,
        0.387731, -0.921773,
        0.403803, -0.914846,
        0.419752, -0.907639,
        0.435572, -0.900154,
        0.451259, -0.892393,
        0.466808, -0.884359,
        0.482214, -0.876054,
        0.497472, -0.867480,
        0.512577, -0.858641,
        0.527526, -0.849539,
        0.542313, -0.840177,
        0.556934, -0.830557,
        0.571384, -0.820683,
        0.585659, -0.810557,
        0.599755, -0.800184,
        0.613667, -0.789565,
        0.627392, -0.778704,
        0.640924, -0.767605,
        0.654259, -0.756270,
        0.667395, -0.744704,
        0.680325, -0.732910,
        0.693048, -0.720892,
        0.705558, -0.708652,
        0.717852, -0.696196,
        0.729926, -0.683526,
        0.741777, -0.670647,
        0.753400, -0.657562,
        0.764793, -0.644276,
        0.775951, -0.630793,
        0.786872, -0.617116,
        0.797552, -0.603251,
        0.807987, -0.589200,
        0.818175, -0.574969,
        0.828112, -0.560563,
        0.837796, -0.545984,
        0.847223, -0.531238,
        0.856390, -0.516329,
        0.865295, -0.501263,
        0.873935, -0.486042,
        0.882308, -0.470673,
        0.890410, -0.455160,
        0.898239, -0.439507,
        0.905793, -0.423719,
        0.913070, -0.407802,
        0.920067, -0.391760,
        0.926783, -0.375598,
        0.933214, -0.359321,
        0.939360, -0.342934,
        0.945217, -0.326441,
        0.950786, -0.309849,
        0.956063, -0.293162,
        0.961047, -0.276385,
        0.965737, -0.259523,
        0.970131, -0.242582,
        0.974228, -0.225567,
        0.978026, -0.208482,
        0.981525, -0.191334,
        0.984723, -0.174127,
        0.987620, -0.156867,
        0.990214, -0.139558,
        0.992505, -0.122207,
        0.994491, -0.104819,
        0.996173, -0.087398,
        0.997550, -0.069950,
        0.998622, -0.052482,
        0.999387, -0.034997,
        0.999847, -0.017501,
        1.000000, -0.000000,
    ]

    def __init__(self):
        super(Helipad, self).__init__()
        self.__load_images()
        self.__load_stored_objects()

    def load_default_coords(self):
        pass

    def draw(self):

        day_display_mood = SharedContent.get_day_display_mood()

        # UPPER PLANE
        glPushMatrix()
        glEnable(GL_TEXTURE_2D)

        # center_x = -0.12
        # center_y = -5.0
        # radius = 0.3

        height1 = 1.2
        # glColor3f(0.5, 0.5, 0.5)
        glBindTexture(GL_TEXTURE_2D, self.__helipad_image)
        glBegin(GL_POLYGON)

        for i in range(0, len(self.__default_vertices_XY), 1):
            # theta = radians(i)
            # x, y, z = center_x + radius * cos(theta), 1.2, center_y + radius * sin(theta)
            # tex_x, tex_y = 0.5 + 0.5 * cos(theta), 0.5 + 0.5 * sin(theta)
            glTexCoord2f(self.__default_tex_coords[i][0], self.__default_tex_coords[i][1])
            glVertex3f(self.__default_vertices_XY[i][0], height1, self.__default_vertices_XY[i][1])

        glEnd()

        glDisable(GL_TEXTURE_2D)
        glPopMatrix()

        # LOWER PLANE

        glPushMatrix()
        height2 = 1.15

        if not day_display_mood:
            glColor3f(0.25, 0.25, 0.75)
        else:
            glColor3f(0.5, 0.5, 0.5)

        glBegin(GL_POLYGON)
        for i in range(0, len(self.__default_vertices_XY), 1):
            glVertex3f(self.__default_vertices_XY[i][0], height2, self.__default_vertices_XY[i][1])

        glEnd()
        glPopMatrix()

        # round plane
        glPushMatrix()

        if not day_display_mood:
            glColor3f(0.50, 0.25, 0.50)
        else:
            glColor3f(0.5, 0.5, 0.5)

        for i in range(0, len(self.__default_vertices_XY) - 1, 1):
            glBegin(GL_POLYGON)
            glVertex3f(self.__default_vertices_XY[i][0], height1, self.__default_vertices_XY[i][1])
            glVertex3f(self.__default_vertices_XY[i][0], height2, self.__default_vertices_XY[i][1])
            glVertex3f(self.__default_vertices_XY[i + 1][0], height2, self.__default_vertices_XY[i + 1][1])
            glVertex3f(self.__default_vertices_XY[i + 1][0], height1, self.__default_vertices_XY[i + 1][1])
            glEnd()

        glPopMatrix()

        # coned
        lower_cone_height = 0.8
        center_x = -0.12
        center_y = -5.0
        glPushMatrix()
        if not day_display_mood:
            glColor3f(0.3, 0.7, 0.3)
        else:
            glColor3f(0.5, 0.5, 0.5)

        for i in range(0, len(self.__default_vertices_XY) - 1, 1):
            glBegin(GL_POLYGON)
            glVertex3f(self.__default_vertices_XY[i][0], height1, self.__default_vertices_XY[i][1])
            glVertex3f(self.__default_vertices_XY[i + 1][0], height1, self.__default_vertices_XY[i + 1][1])
            glVertex3f(center_x, lower_cone_height, center_y)
            glEnd()

        glPopMatrix()

        # width = 0.125
        # lower_cone_height = -width * 7
        #
        # # Upper plate
        # glPushMatrix()
        # glColor3f(1.0, 0.0, 0.0)
        # glBegin(GL_POLYGON)
        # for i in range(0, 360, 2):
        #     glVertex3f(self.__default_vertices_XY[i], -width, self.__default_vertices_XY[i+1])
        # glEnd()
        # glPopMatrix()



        # # Lower Plane
        #
        # glPushMatrix()
        # glColor3f(1.0, 0.0, 0.0)
        # glBegin()
        # for i in range(0, len(self.__default_vertices_XY), 2):
        #     glVertex3f(self.__default_vertices_XY[i], -width, self.__default_vertices_XY[i + 1])
        # glEnd()
        # glPopMatrix()
        #
        #
        # # Rounded Side
        #
        # glPushMatrix()
        # glColor3f(0.0, 0.0, 1.0)
        #
        # for i in range(0, len(self.__default_vertices_XY), 4):
        #     glBegin(GL_POLYGON)
        #     glVertex3f(self.__default_vertices_XY[i], -width, self.__default_vertices_XY[i+1])
        #     glVertex3f(self.__default_vertices_XY[i+2], -width, self.__default_vertices_XY[i+3])
        #     glVertex3f(self.__default_vertices_XY[i+2], width, self.__default_vertices_XY[i+3])
        #     glVertex3f(self.__default_vertices_XY[i], width, self.__default_vertices_XY[i+1])
        # glEnd()
        # glPopMatrix()

        self.__draw_pipe_bars()

    def __load_images(self):
        self.__helipad_image = get_texture('helipad2.jpg')
        self.__test_circular_image = get_texture('frontWheel.jpg')

    def __load_stored_objects(self):
        self.__helipad_objects = StoreKeeper.open_object('helipad_objects')
        self.__default_vertices_XY = self.__helipad_objects['helipad_coords']
        self.__default_tex_coords = self.__helipad_objects['tex_coords']
        self.__pipe_objects = StoreKeeper.open_object('pipe_objects')
        self.__pipe = self.__pipe_objects['pipe']

    def __draw_pipe_bars(self):

        day_display_mood = SharedContent.get_day_display_mood()

        upper_Z = 2.0
        lower_Z = -2.0
        increament = -0.01
        angle = 80.0

        trans_x_factor = 0.0
        trans_y_factor = 0.85
        trans_z_factor = -5.0

        scale_x_factor = 0.02
        scale_y_factor = 0.15
        scale_z_factor = 0.02

        rotate_x_factor = 0.0
        rotate_y_factor = 1.0
        rotate_z_factor = 1.0

        # RIGHT SIDE

        glPushMatrix()
        glTranslatef(trans_x_factor, trans_y_factor, trans_z_factor)
        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if day_display_mood:
            glColor3f(0.2, 0.2, 0.2)
        else:
            glColor3f(0.7, 0.2, 0.5)

        for i in range(len(self.__pipe)):
            # print(self.__default_vertices_XY[i], lower_Z, self.__default_vertices_XY[i+1])
            # print(self.__default_vertices_XY[i+2], lower_Z, self.__default_vertices_XY[i+3])
            # print(self.__default_vertices_XY[i+2], upper_Z, self.__default_vertices_XY[i+3])
            # print(self.__default_vertices_XY[i], upper_Z, self.__default_vertices_XY[i+1])
            glBegin(GL_POLYGON)
            for j in range(len(self.__pipe[i])):
                glVertex(self.__pipe[i][j][0], self.__pipe[i][j][1], self.__pipe[i][j][2])
            glEnd()

        glFlush()
        glPopMatrix()

        angle = -78.0

        trans_x_factor = -0.28
        trans_y_factor = 0.78
        trans_z_factor = -5.0

        scale_x_factor = 0.02
        scale_y_factor = 0.15
        scale_z_factor = 0.02

        rotate_x_factor = 0.0
        rotate_y_factor = 1.0
        rotate_z_factor = 1.0

        glPushMatrix()
        glTranslatef(trans_x_factor, trans_y_factor, trans_z_factor)
        glScalef(scale_x_factor, scale_y_factor, scale_z_factor)
        glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)

        if day_display_mood:
            glColor3f(0.2, 0.2, 0.2)
        else:
            glColor3f(0.7, 0.2, 0.5)

        for i in range(len(self.__pipe)):
            # print(self.__default_vertices_XY[i], lower_Z, self.__default_vertices_XY[i+1])
            # print(self.__default_vertices_XY[i+2], lower_Z, self.__default_vertices_XY[i+3])
            # print(self.__default_vertices_XY[i+2], upper_Z, self.__default_vertices_XY[i+3])
            # print(self.__default_vertices_XY[i], upper_Z, self.__default_vertices_XY[i+1])
            glBegin(GL_POLYGON)
            for j in range(len(self.__pipe[i])):
                glVertex(self.__pipe[i][j][0], self.__pipe[i][j][1], self.__pipe[i][j][2])
            glEnd()

        glFlush()
        glPopMatrix()
