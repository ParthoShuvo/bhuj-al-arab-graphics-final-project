


__object_to_image_map__ = {
    'WATER': 'water_image.png',
    'TRIANGLE_ISLAND_OBJECT': 'triangle_island.bmp'
}


def get_image_name_by_object(object_type):
    return __object_to_image_map__[object_type]

