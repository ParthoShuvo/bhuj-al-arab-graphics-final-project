from github.parthoshuvo.objects.BaseObject import BaseObject
from github.parthoshuvo.store_keeper import StoreKeeper
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


class TriangleShapeIsland(BaseObject):
    __default_vertices = [
        -0.1, 3.3, 0.0,
        -0.6, 3.0, 0.0,
        -0.7, 2.9, 0.0,
        -1.0, 2.4, 0.0,
        -1.1, 2.2, 0.0,
        -1.2, 2.0, 0.0,
        -1.4, 1.7, 0.0,
        -1.5, 1.5, 0.0,
        -1.6, 1.4, 0.0,
        -1.7, 1.2, 0.0,
        -1.9, 0.9, 0.0,
        -2.0, 0.7, 0.0,
        -2.2, 0.4, 0.0,
        -2.3, 0.2, 0.0,
        -2.5, -0.1, 0.0,
        -2.6, -0.3, 0.0,
        -2.8, -0.6, 0.0,
        -2.9, -0.8, 0.0,
        -3.0, -1.3, 0.0,
        -3.0, -1.4, 0.0,
        -2.9, -1.7, 0.0,
        -2.8, -1.9, 0.0,
        -2.7, -2.0, 0.0,
        -2.3, -2.4, 0.0,
        -2.2, -2.5, 0.0,
        -2.0, -2.6, 0.0,
        -1.5, -2.8, 0.0,
        -1.4, -2.83, 0.0,
        -1.3, -2.9, 0.0,
        -0.57, -2.97, 0.0,  # Was: y - 2.9
        -0.5, -3.0, 0.0,
        -0.4, -3.0, 0.0,
        -0.3, -3.0, 0.0,
        -0.2, -3.0, 0.0,
        -0.1, -3.0, 0.0,
        0.0, -3.0, 0.0,
        0.1, -3.0, 0.0,
        0.2, -3.0, 0.0,
        0.3, -3.0, 0.0,
        0.4, -3.0, 0.0,
        0.5, -3.0, 0.0,
        0.6, -3.0, 0.0,
        0.7, -3.0, 0.0,
        1.4, -2.9, 0.0,
        1.9, -2.8, 0.0,
        2.2, -2.72, 0.0,
        2.5, -2.6, 0.0,
        2.8, -2.4, 0.0,
        2.9, -2.33, 0.0,
        3.2, -2.15, 0.0,
        3.5, -1.5, 0.0,
        3.5, -0.8, 0.0,
        3.3, -0.39, 0.0,
        3.4, -0.6, 0.0,
        3.1, -0.1, 0.0,
        2.9, 0.2, 0.0,
        2.7, 0.5, 0.0,
        2.5, 0.8, 0.0,
        2.3, 1.1, 0.0,
        2.1, 1.4, 0.0,
        2.0, 1.5, 0.0,
        1.9, 1.7, 0.0,
        1.7, 2.0, 0.0,
        1.5, 2.3, 0.0,
        1.2, 2.72, 0.0,
        1.1, 2.9, 0.0,
        1.0, 3.0, 0.0,
        0.9, 3.1, 0.0,
        0.8, 3.2, 0.0,  # was: x: 0.3
        0.5, 3.3, 0.0,
        0.0, 3.3, 0.0,
    ]

    def __init__(self):
        super(TriangleShapeIsland, self).__init__()
        self.__load_stored_objects()

    def load_default_coords(self):
        pass

    def draw(self):
        y = -2.5
        z = -5.0
        angle = 180.0

        glDisable(GL_TEXTURE_2D)
        glEnable(GL_MULTISAMPLE)
        glDisable(GL_LIGHTING)

        #
        # glRotate(-180.0, 0.0, 1.0, 0.0)
        #
        # glRotate(45.0, 1.0, 0.0, 0.0)
        glPushMatrix()
        scale_x_factor = 1.0
        scale_y_factor = 1.0
        glTranslatef(0.0, y, z)
        glRotatef(angle, 0, 1, 0)
        glRotatef(90.0, 1, 0, 0)
        glScalef(scale_x_factor, scale_y_factor, 1.0)
        # Drawing the upper face

        glColor3f(0.01, 0.05, 0.03)
        # glTranslatef(0.0, 0.0, 0.0)
        glBegin(GL_POLYGON)
        for i in range(len(self.__upper_face)):
            glVertex3f(self.__upper_face[i][0], self.__upper_face[i][1], self.__upper_face[i][2])
        glEnd()
        glPopMatrix()

        # Drawing dark-radish plane
        glPushMatrix()
        scale_x_factor = 1.5
        scale_y_factor = 1.5
        glColor3f(0.35, 0.05, 0.1)
        glTranslatef(0.0, y, z)
        glRotatef(angle, 0, 1, 0)
        glRotatef(90.0, 1, 0, 0)
        glScalef(scale_x_factor, scale_y_factor, 1.01)

        # glTranslatef(0.0, 0.0, -0.2)
        glBegin(GL_POLYGON)
        for i in range(len(self.__dark_reddish_face)):
            # printf("%f, %f, %f\n", vertices[i], vertices[i+1], vertices[i+2]);
            glVertex3f(self.__dark_reddish_face[i][0], self.__dark_reddish_face[i][1], self.__dark_reddish_face[i][2])
        glEnd()
        glPopMatrix()

        # Drawing Greenish plane
        glPushMatrix()
        scale_x_factor = 1.15
        scale_y_factor = 1.15
        glColor3f(0.0, 0.3, 0.1)
        glTranslatef(0.0, y, z)
        glRotatef(angle, 0, 1, 0)
        glRotatef(90.0, 1, 0, 0)
        glScalef(scale_x_factor, scale_y_factor, 1.02)

        # glTranslatef(0.0, 0.0, -0.2)
        glBegin(GL_POLYGON)
        for i in range(len(self.__dark_greenish_face)):
            # printf("%f, %f, %f\n", vertices[i], vertices[i+1], vertices[i+2]);
            glVertex3f(self.__dark_greenish_face[i][0], self.__dark_greenish_face[i][1],
                       self.__dark_greenish_face[i][2])
        glEnd()
        glPopMatrix()

        # Drawing the lower face
        glPushMatrix()
        scale_x_factor = 1.5
        scale_y_factor = 1.5
        # glTranslatef(0.0, 0.0, -5.0)
        glColor3f(0.3, 0.7, 0.7)
        glTranslatef(0.0, y, z)
        glRotatef(angle, 0, 1, 0)
        glRotatef(90.0, 1, 0, 0)
        glScalef(scale_x_factor, scale_y_factor, 1.01)


        glBegin(GL_POLYGON)
        for i in range(len(self.__lower_face)):
            # printf("%f, %f, %f\n", vertices[i], vertices[i+1], vertices[i+2]);
            glVertex3f(self.__lower_face[i][0], self.__lower_face[i][1], self.__lower_face[i][2])
        glEnd()
        glPopMatrix()


        # Drawing the side-face
        glPushMatrix()
        scale_x_factor = 1.5
        scale_y_factor = 1.5
        glColor3f(0.0, 0.0, 0.7)
        glTranslatef(0.0, y, z)
        glRotatef(angle, 0, 1, 0)
        glRotatef(90.0, 1, 0, 0)
        glScalef(scale_x_factor, scale_y_factor, 1.01)
        glBegin(GL_POLYGON)
        for i in range(len(self.__side_face)):
            # printf("%f, %f, %f\n", vertices[i], vertices[i+1], vertices[i+2]);
            glVertex3f(self.__side_face[i][0], self.__side_face[i][1], self.__side_face[i][2])
        glEnd()
        glPopMatrix()

        glDisable(GL_MULTISAMPLE)



    def __load_stored_objects(self):
        self.__platform_objects = StoreKeeper.open_object('platform_objects')
        self.__upper_face = self.__platform_objects['upper_face']
        self.__dark_reddish_face = self.__platform_objects['dark_reddish_face']
        self.__dark_greenish_face = self.__platform_objects['dark_greenish_face']
        self.__lower_face = self.__platform_objects['lower_face']
        self.__side_face = self.__platform_objects['side_face']



        # def __calculate_plane_coordinates(self, plane_type, given_coordinates, z_axis_val=None):
        #     new_coordinates = OrderedDict()
        #     given_coordinates = self.__default_original_coordinates if not given_coordinates else given_coordinates
        #     if plane_type in ['UPPER_FACE_PLANE', 'DARK_REDDISH_PLANE', 'GREENISH_PLANE', 'BOTTOM_PLANE']:
        #         len = len(given_coordinates) * 3
        #         for i in range(0, len, 3):
        #             new_coordinates[i] = [given_coordinates[i]/3, given_coordinates[i+1]/3, z_axis_val]
        #
        #     elif plane_type == 'SIDE_FACE_PLANE':
        #         len = (len(given_coordinates)-1) * 3
        #         z_axis_val = fabs(z_axis_val)
        #         for i in range(0, len, 3):
        #             glVertex3f(given_coordinates[i] / 3, given_coordinates[i + 1] / 3, -z_axis_val)
        #             glVertex3f(given_coordinates[i] / 3, given_coordinates[i + 1] / 3, z_axis_val)
        #             glVertex3f(given_coordinates[i + 3] / 3, given_coordinates[i + 4] / 3, z_axis_val)
        #             glVertex3f(given_coordinates[i + 3] / 3, given_coordinates[i + 4] / 3, -z_axis_val)
