import path
import os
import shelve

__object_store_folder_path__ = os.path.join(path.__root__, os.path.join('saved_files', 'objects'))

def store_object(object, object_name):
    s = shelve.open(os.path.join(__object_store_folder_path__, object_name), 'c')
    s[object_name] = object
    s.close()

def open_object(object_name):
    s = shelve.open(os.path.join(__object_store_folder_path__, object_name), 'r')
    object = s[object_name]
    s.close()
    return object