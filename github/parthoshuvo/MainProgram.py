import shelve
from collections import OrderedDict
from math import *

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from github.parthoshuvo.objects.CloudObject import CloudObject
from github.parthoshuvo.objects.WaterObject import WaterObject
from github.parthoshuvo.objects.OuterFrame import OuterFrame
from github.parthoshuvo.objects.Helipad import Helipad
from github.parthoshuvo.objects.TriangleShapeIsland import TriangleShapeIsland
from github.parthoshuvo.objects.Pipe import Pipe
from github.parthoshuvo.objects.InnerBody import InnerBody
import os
import shelve
from github.parthoshuvo.store_keeper import StoreKeeper
from github.parthoshuvo.opengl.common.SharedContent import SharedContent


class MainProgram(object):
    __water_obj = None
    __cloud_obj = None
    __outer_frame_obj = None
    __triangle_island_obj = None
    __island_upper_face_obj = None
    __island_reddish_face_obj = None
    __island_greenish_face_obj = None
    __island_bottom_face_obj = None
    __island_side_face_obj = None
    __pipe_obj = None
    __inner_body_obj = None
    __helipad_obj = None


    __camera_rot_flag_x = 0.0
    __camera_rot_flag_y = 1.0
    __camera_rot_flag_z = 0.0
    __camera_rot_angle = 0.0

    _angle = 180.0
    __dummy_angle = 30.0

    __eye_point_angle = 45.0
    __zooming_factor = 0.5

    __camera_eye_vector = [0.0, 0.0, 4.5]
    __screen_centre_vector = [0.0, 0.0, 0.0]
    __camera_up_vector = [0.0, 1.0, 0.0]

    __view_mode = 'FRONT'

    __round_360_trip_flag = False
    __round_360_trip_ind = 0

    # __day_display_flag = True  # TRUE FOR DAY AND FALSE FOR NIGHT



    """
    constructor
    """

    def __init__(self, scr_width=640, scr_height=480, window_name='OPEN-GL GRAPHICS PROJECT'):
        self.__scr_width = scr_width
        self.__scr_height = scr_height
        self.__window_name = window_name
        self.__coord_left = -1.0
        self.__coord_right = 1.0
        self.__coord_top = 1.0
        self.__coord_bottom = -1.0
        self.__near = 1.0
        self.__far = 200.0
        self.__load_stored_data()

    def __zoom_in(self):
        if self.__view_mode == 'FRONT':
            if self.__camera_eye_vector[2] > 1.0:
                self.__camera_eye_vector[2] -= self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[2]))
        elif self.__view_mode == 'TOP-BACK' or self.__view_mode == 'TOP-FRONT':
            if self.__camera_eye_vector[1] > 2.5:
                self.__camera_eye_vector[1] -= self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[1]))
        elif self.__view_mode == 'LEFT':
            if self.__camera_eye_vector[0] < -6.0:
                self.__camera_eye_vector[0] += self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[0]))
        elif self.__view_mode == 'RIGHT':
            if self.__camera_eye_vector[0] > 6.0:
                self.__camera_eye_vector[0] -= self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[0]))
        elif self.__view_mode == 'BACK':
            if self.__camera_eye_vector[2] < -11.5:
                self.__camera_eye_vector[2] += self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[2]))
        elif self.__view_mode == 'FRONT-RIGHT':
            if self.__camera_eye_vector[0] > 8.0:
                self.__camera_eye_vector[0] -= self.__zooming_factor
            if self.__camera_eye_vector[2] > -1.0:
                self.__camera_eye_vector[2] -= self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[0]) + " " + str(self.__camera_eye_vector[2]))
        elif self.__view_mode == 'FRONT-LEFT':
            if self.__camera_eye_vector[0] < -8.0:
                self.__camera_eye_vector[0] += self.__zooming_factor
            if self.__camera_eye_vector[2] > 0.5:
                self.__camera_eye_vector[2] -= self.__zooming_factor
            print('Zoom IN: ' + str(self.__camera_eye_vector[0]) + " " + str(self.__camera_eye_vector[2]))

    def __zoom_out(self):
        if self.__view_mode == 'FRONT':
            if self.__camera_eye_vector[2] < 13.0:
                self.__camera_eye_vector[2] += self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[2]))
        elif self.__view_mode == 'TOP-BACK' or self.__view_mode == 'TOP-FRONT':
            if self.__camera_eye_vector[1] < 11.5:
                self.__camera_eye_vector[1] += self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[1]))
        elif self.__view_mode == 'LEFT':
            if self.__camera_eye_vector[0] > -16.0:
                self.__camera_eye_vector[0] -= self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[0]))
        elif self.__view_mode == 'RIGHT':
            if self.__camera_eye_vector[0] < 16.0:
                self.__camera_eye_vector[0] += self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[0]))
        elif self.__view_mode == 'BACK':
            if self.__camera_eye_vector[2] > -21.0:
                self.__camera_eye_vector[2] -= self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[2]))
        elif self.__view_mode == 'FRONT-RIGHT':
            if self.__camera_eye_vector[0] < 15.0:
                self.__camera_eye_vector[0] += self.__zooming_factor
            if self.__camera_eye_vector[2] < 6.0:
                self.__camera_eye_vector[2] += self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[0]) + " " + str(self.__camera_eye_vector[2]))
        elif self.__view_mode == 'FRONT-LEFT':
            if self.__camera_eye_vector[0] > -13.5:
                self.__camera_eye_vector[0] -= self.__zooming_factor
            if self.__camera_eye_vector[2] < 6.0:
                self.__camera_eye_vector[2] += self.__zooming_factor
            print('Zoom OUT: ' + str(self.__camera_eye_vector[0]) + " " + str(self.__camera_eye_vector[2]))

    def __process_special_keys(self, key, x, y):
        if key == GLUT_KEY_UP:
            self.__zoom_in()
            return

        elif key == GLUT_KEY_DOWN:
            self.__zoom_out()
            return

        elif key == GLUT_KEY_PAGE_UP:
            self.__view_mode = 'TOP-FRONT'
            self.__camera_eye_vector = [0.0, 10.0, -4.9]
            self.__screen_centre_vector = [0.0, -5.0, -5.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        elif key == GLUT_KEY_PAGE_DOWN:
            self.__view_mode = 'TOP-BACK'
            self.__camera_eye_vector = [0.0, 10.0, -5.0]
            self.__screen_centre_vector = [0.0, -5.0, -4.9]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        elif key == GLUT_KEY_LEFT:
            self.__view_mode = 'LEFT'
            self.__camera_eye_vector = [-20.0, 0.0, -5.0]
            self.__screen_centre_vector = [15.0, 0.0, -5.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        elif key == GLUT_KEY_RIGHT:
            self.__view_mode = 'RIGHT'
            self.__camera_eye_vector = [15.0, 0.0, -5.0]
            self.__screen_centre_vector = [-20.0, 0.0, -5.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        elif key == GLUT_KEY_F5:
            self.__view_mode = 'FRONT'
            self.__reset_camera_look_pos()
            return

    def __reset_camera_look_pos(self):
        self.__camera_eye_vector = [0.0, 0.0, 4.5]
        self.__screen_centre_vector = [0.0, 0.0, 0.0]
        self.__camera_up_vector = [0.0, 1.0, 0.0]

    def __process_mouse_click(self, button, state, x, y):
        print('mouse clicked')
        if button == GLUT_LEFT_BUTTON:
            print("Before: ", SharedContent.get_day_display_mood())
            SharedContent.toggle_day_display_mood()
            print("Before: ", SharedContent.get_day_display_mood())
            return



    def __process_keyboard(self, key, x, y):

        # DAY / NIGHT MOOD
        if key == b'D' or key == b'd':
            print("Before: ", SharedContent.get_day_display_mood())
            SharedContent.toggle_day_display_mood()
            print("Before: ", SharedContent.get_day_display_mood())
            return


        # RIGHT VIEW
        elif key == b'R' or key == b'r':
            self.__view_mode = 'RIGHT'
            self.__camera_eye_vector = [15.0, 0.0, -5.0]
            self.__screen_centre_vector = [-20.0, 0.0, -5.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        # LEFT VIEW
        elif key == b'L' or key == b'l':
            self.__view_mode = 'LEFT'
            self.__camera_eye_vector = [-20.0, 0.0, -5.0]
            self.__screen_centre_vector = [15.0, 0.0, -5.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        # 360 TOUR VIEW
        elif key == b'T' or key == b't':
            self.__view_mode = '360-TOUR'
            self.__round_360_trip_flag = not self.__round_360_trip_flag
            if self.__round_360_trip_flag:
                self.__round_360_trip_ind = 0
            return

        # FRONT VIEW
        elif key == b'F' or key == b'f':
            self.__view_mode = 'FRONT'
            self.__reset_camera_look_pos()

        # BACK VIEW
        elif key == b'B' or key == b'b':
            self.__view_mode = 'BACK'
            self.__camera_eye_vector = [0.0, 0.0, -15.0]
            self.__screen_centre_vector = [0.0, 0.0, 0.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        # FRONT RIGHT
        elif key == b'w' or key == b'W':
            self.__view_mode = 'FRONT-RIGHT'
            self.__camera_eye_vector = [11.5, 1.5, 2.5]
            self.__screen_centre_vector = [-5.0, 1.5, -8.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        # FRONT LEFT
        elif key == b'q' or key == b'Q':
            self.__view_mode = 'FRONT-LEFT'
            self.__camera_eye_vector = [-11.0, 1.5, 3.5]
            self.__screen_centre_vector = [-5.0, 1.5, -2.0]
            self.__camera_up_vector = [0.0, 1.0, 0.0]
            return

        print('VIEWING MODE: ' + self.__view_mode)

    def __update_round_360_degree(self, val):
        if self.__round_360_trip_flag:
            self.__round_360_trip_ind = 0 if self.__round_360_trip_ind >= 360 else self.__round_360_trip_ind
            camera_eye = self.__round_360_coord[self.__round_360_trip_ind]
            screen_centre_position = [0.0, -1.0, -5.0]
            self.__camera_eye_vector[0] = camera_eye[0]
            self.__camera_eye_vector[1] = camera_eye[1]
            self.__camera_eye_vector[2] = camera_eye[2]
            self.__screen_centre_vector[0] = screen_centre_position[0]
            self.__screen_centre_vector[1] = screen_centre_position[1]
            self.__screen_centre_vector[2] = screen_centre_position[2]
            self.__round_360_trip_ind += 1
        glutPostRedisplay()
        glutTimerFunc(3, self.__update_round_360_degree, 0)

    def __load_stored_data(self):
        # path = os.path.join(os.path.join('/home/shuvojit/PycharmProjects/DataSetFileI/Bhuj Al Arab', 'saved_files'), 'objects')
        self.__round_360_coord = StoreKeeper.open_object('round_360_coord')
        print('saved data loaded', len(self.__round_360_coord))

    """
    start program
    """

    def run(self):
        self.__load_stored_data()
        self.__init_graphics_env()

    def __init_display_objects(self):
        self.__water_obj = WaterObject(WaterObject.get_default_original_coords(), WaterObject
                                       .get_default_texture_coords(), WaterObject.get_default_normal_vector(),
                                       'water.bmp')
        self.__cloud_obj = CloudObject(CloudObject.get_default_original_coords(), CloudObject
                                       .get_default_texture_coords(), None,
                                       'cloud02.bmp')
        self.__outer_frame_obj = OuterFrame()
        self.__triangle_island_obj = TriangleShapeIsland()
        self.__pipe_obj = Pipe()
        self.__inner_body_obj = InnerBody()
        self.__helipad_obj = Helipad()
        # self.__water_obj = ObjectsFactory.get_object(ObjectsFactory.__WATER_OBJECT__)
        # self.__water_obj.load_default_coords()
        # self.__water_obj.test_object_member_value()
        # self.__triangle_island_obj = ObjectsFactory.get_object(ObjectsFactory.__TRIANGLE_ISLAND_OBJECT__)
        # self.__triangle_island_obj.load_default_coords()
        # self.__triangle_island_obj.test_object_member_value()

    def set_display_coordinate_in_3d(self, left=-1.0, right=1.0, top=1.0, bottom=-1.0, near=1.0, far=200.0):
        self.__coord_left = left
        self.__coord_right = right
        self.__coord_top = top
        self.__coord_bottom = bottom
        tmp_near = fabs(near)
        tmp_far = fabs(far)
        if tmp_near >= 1.0 and tmp_far >= 1.0:
            self.__near = tmp_near
            self.__far = tmp_far
        else:
            # TODO raise an exception here
            pass

    """
    init graphics environment
    """

    def __init_graphics_env(self):
        glutInit()  # initialize glut
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH)
        glutInitWindowSize(self.__scr_width, self.__scr_height)  # set window size
        glutInitWindowPosition(0, 0)  # set window position
        print('###### graphics environment has been setup ########')
        glutCreateWindow(self.__window_name)  # create window with title
        self.__init_display_rendering()
        self.__init_display_objects()
        glutDisplayFunc(self.__draw_objects)  # set draw function callback
        glutIdleFunc(self.__draw_objects)  # draw all the time
        glutKeyboardFunc(self.__process_keyboard)  # keyboard interrupt
        glutMouseFunc(self.__process_mouse_click)
        glutSpecialFunc(self.__process_special_keys)
        glutReshapeFunc(self.__reshape_drawing_screen)
        glutTimerFunc(25, self.update, 0)
        glutTimerFunc(25, self.__update_round_360_degree, 0)
        glutMainLoop()  # a start everything

    def __interrupt_special_keyboard(self, key, x, y):
        pass

    def __update_camera_rotation(self, camera_rot_angle, flag_x_axis, flag_y_axis, flag_z_axis):
        self.__camera_rot_angle = camera_rot_angle
        self.__camera_rot_flag_x = flag_x_axis
        self.__camera_rot_flag_y = flag_y_axis
        self.__camera_rot_flag_z = flag_z_axis

    def __init_display_rendering(self):
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_NORMALIZE)
        glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE)
        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)
        glShadeModel(GL_SMOOTH)

        print('##### initialize display rendering ######')

    def __rotate_camera(self):
        glRotate(self.__camera_rot_angle, self.__camera_rot_flag_x, self.__camera_rot_flag_y, self.__camera_rot_flag_z)
        glTranslatef(0.0, 0.0, -5.0)
        # glRotatef(10.0, 1.0, 0.0, 0.0)
        # glRotatef(-10.0, 0.0, 1.0, 0.0)
        # glRotatef(self.__camera_rot_angle, 0.0, 0.0, 1.0)

    def __reshape_drawing_screen(self, scr_width, scr_height):
        glViewport(0, 0, scr_width, scr_height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        # ratio = float(scr_width) / float(scr_height)
        gluPerspective(self.__eye_point_angle, self.__scr_width / self.__scr_height,
                       self.__near, self.__far)
        # glFrustum(self.__coord_left, self.__coord_right, self.__coord_bottom, self.__coord_top, self.__near, self.__far)

    def __timer(self, val):
        # self.__dummy_angle += 1.0
        # if self.__dummy_angle >= 360.0:
        #     self.__dummy_angle = 0
        glutPostRedisplay()
        glutTimerFunc(25, self.__timer, 0)

    def __clear_screen(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clear the screen
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()  # reset the position

    def __set_lightening(self):
        pass
        # glEnable(GL_LIGHTING)
        # ambientColor = [1.0, 1.0, 1.0, 1.0]
        # glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor)
        # glDisable(GL_LIGHTING)

    def __draw_objects(self):
        self.__clear_screen()
        gluLookAt(self.__camera_eye_vector[0], self.__camera_eye_vector[1], self.__camera_eye_vector[2],
                  self.__screen_centre_vector[0], self.__screen_centre_vector[1], self.__screen_centre_vector[2],
                  self.__camera_up_vector[0], self.__camera_up_vector[1], self.__camera_up_vector[2])

        self.__set_lightening()

        glColor3f(1.0, 1.0, 1.0)
        self.__water_obj.draw()

        # DRAW TRIANGLE PLATFORM
        glColor3f(1.0, 1.0, 1.0)
        self.__triangle_island_obj.draw()

        # glDisable(GL_TEXTURE_2D)
        glColor3f(1.0, 1.0, 1.0)
        self.__outer_frame_obj.draw()

        glColor3f(1.0, 1.0, 1.0)
        self.__cloud_obj.draw()

        # glDisable(GL_TEXTURE_2D)
        glColor3f(1.0, 1.0, 1.0)
        self.__pipe_obj.draw()

        glColor3f(1.0, 1.0, 1.0)
        self.__inner_body_obj.draw()

        glColor3f(1.0, 1.0, 1.0)
        self.__helipad_obj.draw()

        glutSwapBuffers()

    def __draw_surround_glasses(self):
        ### LEFT PLANE
        glPushMatrix()
        glColor3f(0.5, 0.5, 0.5)
        glBegin(GL_QUADS)
        glVertex3f(-3.0, -10.0, -5.0)  # A
        glVertex3f(-0.2, -10.0, -8.0)  # B
        glVertex3f(-0.2, 16.0, -8.0)  # C
        glVertex3f(-3.0, 16.0, -5.0)  # D

        ### RIGHT PLANE
        glVertex3f(0.2, -10.0, -8.0)  # A
        glVertex3f(3.0, -10.0, -5.0)  # B
        glVertex3f(3.0, 16.0, -5.0)  # C
        glVertex3f(0.2, 16.0, -8.0)  # D
        glEnd()
        glPopMatrix()

        ### TOP PLANE
        glPushMatrix()
        glColor3f(0.3, 0.3, 0.3)
        glBegin(GL_QUADS)
        glVertex3f(-3.0, 16.0, -5.0)  # A
        glVertex3f(3.0, 16.0, -5.0)  # B
        glVertex3f(0.2, 16.0, -8.0)  # C
        glVertex3f(-0.2, 16.0, -8.0)  # D
        glEnd()
        glPopMatrix()

        ## FRONT PLANE

        ### TRY TO DRAW A TRIANGLE FROM LEFT SIDE
        glPushMatrix()
        glColor3f(0.4, 0.4, 0.0)
        glBegin(GL_TRIANGLES)
        glVertex3f(-3.2, -10.0, -3.0)  # A
        glVertex3f(-3.0, -10.0, -5.0)  # B
        glVertex3f(-3.0, 16.0, -5.0)  # C

        glVertex3f(-3.1, 16.0, -3.8)  # A
        glVertex3f(-3.0, -10.0, -5.0)  # B
        glVertex3f(-3.0, 16.0, -5.0)  # D

        ### TRY TO DRAW A TRIANGLE FROM RIGHT SIDE
        glVertex3f(3.2, -10.0, -3.)  # A
        glVertex3f(3.0, -10.0, -5.0)  # B
        glVertex3f(3.0, 16.0, -5.0)  # C

        glVertex3f(3.2, 16.0, -3.8)  # A
        glVertex3f(3.0, -10.0, -5.0)  # B
        glVertex3f(3.0, 16.0, -5.0)  # C

        glEnd()
        glPopMatrix()

        # TODAYS' WORK
        # glPushMatrix()
        # glColor3f(0.3, 0.1, 0.2)
        # # TOP TO BOTTOM
        # # ----------------------------
        # glBegin(GL_QUADS)
        # glVertex3f(-3.5, 12.0, -4.8)  # A
        # glVertex3f(3.5, 12.0, -4.8)  # B
        # glVertex3f(3.0, 16.0, -5.0)  # C
        # glVertex3f(-3.0, 16.0, -5.0)  # D
        #
        # # JOIN TO LEFT PLANE
        # glVertex3f(-3.0, 12.0, -5.0)  # A
        # glVertex3f(-3.5, 12.0, -4.8)  # B
        # glVertex3f(-3.0, 16.0, -5.0)  # C
        # glVertex3f(-3.0, 16.0, -5.0)  # D
        #
        # # JOIN TO RIGHT PLANE
        # glVertex3f(3.5, 12.0, -4.8)  # A
        # glVertex3f(3.0, 12.0, -5.0)  # B
        # glVertex3f(3.0, 16.0, -5.0)  # C
        # glVertex3f(3.0, 16.0, -5.0)  # D
        #
        # # ------------------------------
        #
        # # BOTTOM TO TOP
        # # ------------------------------
        # glVertex3f(-3.8, -10.0, -4.5)  # A
        # glVertex3f(3.8, -10.0, -4.5)  # B
        # glVertex3f(4.2, -5.0, -4.0)  # C
        # glVertex3f(-4.2, -5.0, -4.0)  # D
        #
        # # JOIN TO LEFT PLANE
        # glVertex3f(-3.0, -10.0, -5.0)  # A
        # glVertex3f(-3.8, -10.0, -4.5)  # B
        # glVertex3f(-4.2, -5.0, -4.0)  # C
        # glVertex3f(-3.0, -5.0, -5.0)  # D
        #
        # # JOIN TO RIGHT PLANE
        # glVertex3f(3.8, -10.0, -4.5)  # A
        # glVertex3f(3.0, -10.0, -5.0)  # B
        # glVertex3f(3.0, -5.0, -5.0)  # C
        # glVertex3f(4.2, -5.0, -4.0)  # D
        # # ----------------------------------
        # glEnd()
        # glPopMatrix()
        #
        # # FRONT-CURVE PLANE
        # # ------------------------------------
        # glPushMatrix()
        # glColor3f(1.0, 0.2, 0.5)
        # glBegin(GL_QUADS)
        # glVertex3f(-4.3, -4.8, -3.8)  # A
        # glVertex3f(4.3, -4.8, -3.8)  # B
        # glVertex3f(4.50, -4.7, -3.7)  # C
        # glVertex3f(-4.50, -4.7, -3.7)  # D
        #
        # glVertex3f(-4.50, -4.7, -3.7)  # A
        # glVertex3f(4.50, -4.7, -3.7)  # B
        # glVertex3f(4.505, -4.6, -3.6)  # C
        # glVertex3f(-4.505, -4.6, -3.6)  # D
        #
        # glVertex3f(-4.505, -4.6, -3.6)  # A
        # glVertex3f(4.505, -4.6, -3.6)  # B
        # glVertex3f(4.51, -4.5, -3.5)  # C
        # glVertex3f(-4.51, -4.5, -3.5)  # D
        #
        # glVertex3f(-4.51, -4.5, -3.5)  # A
        # glVertex3f(4.51, -4.5, -3.5)  # B
        # glVertex3f(4.515, -4.4, -3.4)  # C
        # glVertex3f(-4.515, -4.4, -3.4)  # D
        # glEnd()
        # glPopMatrix()
        #
        # self.__draw_curve_steps_bottom_to_top(-4.515, -4.4, -3.4, 0.005, 0.1, 0.1, 30)
        # self.__draw_curve_steps_bottom_to_top(-4.660, -1.4, -0.4, 0.0, 0.1, 0.0, 30)

        # ------------------------------------

    def __draw_curve_steps_bottom_to_top(self, x, y, z, incr_x, incr_y, incr_z, steps):
        glPushMatrix()
        glColor3f(0.1, 0.2, 0.05)
        glBegin(GL_QUADS)
        for i in range(0, steps):
            glVertex(x, y, z)
            glVertex(-x, y, z)
            x += incr_x
            y += incr_y
            z += incr_z
            glVertex(-x, y, z)
            glVertex(x, y, z)
        glEnd()
        glPopMatrix()

    def __draw_frame(self):

        ### VERTICAL PORTION --- A

        glPushMatrix()
        # glRotate(self.__dummy_angle, 0.0, 0.0, 1.0)
        glBegin(GL_QUADS)
        glColor3f(1.0, 1.0, 1.0)
        # front
        glVertex3f(-0.2, -10.0, -8.0)  # A
        glVertex3f(0.2, -10.0, -8.0)  # B
        glVertex3f(0.2, 16.0, -8.0)  # C
        glVertex3f(-0.2, 16.0, -8.0)  # D

        # back
        glVertex3f(-0.2, -10.0, -8.5)  # A
        glVertex3f(0.2, -10.0, -8.5)  # B
        glVertex3f(0.2, 16.0, -8.5)  # C
        glVertex3f(-0.2, 16.0, -8.5)  # D

        # right
        glVertex3f(0.2, -10.0, -8.5)  # A
        glVertex3f(0.2, -10.0, -8.0)  # B
        glVertex3f(0.2, 18.0, -8.0)  # C
        glVertex3f(0.2, 18.0, -8.5)  # D

        # left
        glVertex3f(-0.2, -10.0, -8.5)  # A
        glVertex3f(-0.2, -10.0, -8.0)  # B
        glVertex3f(-0.2, 18.0, -8.0)  # C
        glVertex3f(-0.2, 18.0, -8.5)  # D

        ### VERTICAL PORTION --- B

        # front
        glVertex3f(-0.1, 16.0, -8.2)  # A
        glVertex3f(0.1, 16.0, -8.2)  # B
        glVertex3f(0.1, 25.0, -8.2)  # C
        glVertex3f(-0.1, 25.0, -8.2)  # D

        # back
        glVertex3f(-0.1, 16.0, -8.4)  # A
        glVertex3f(0.1, 16.0, -8.4)  # B
        glVertex3f(0.1, 25.0, -8.4)  # C
        glVertex3f(-0.1, 25.0, -8.4)  # D

        # right
        glVertex3f(0.1, 16.0, -8.4)  # A
        glVertex3f(0.1, 16.0, -8.2)  # B
        glVertex3f(0.1, 25.0, -8.2)  # C
        glVertex3f(0.1, 25.0, -8.4)  # D

        # left
        glVertex3f(-0.1, 16.0, -8.4)  # A
        glVertex3f(-0.1, 16.0, -8.2)  # B
        glVertex3f(-0.1, 25.0, -8.2)  # C
        glVertex3f(-0.1, 25.0, -8.4)  # D

        ### Horizontal Portion

        # 1
        glVertex3f(-0.2, 16.5, -8.3)  # A
        glVertex3f(0.2, 16.5, -8.3)  # B
        glVertex3f(0.2, 16.8, -8.3)  # C
        glVertex3f(-0.2, 16.8, -8.3)  # D

        # 2
        glVertex3f(-0.2, 17.0, -8.3)  # A
        glVertex3f(0.2, 17.0, -8.3)  # B
        glVertex3f(0.2, 17.3, -8.3)  # C
        glVertex3f(-0.2, 17.3, -8.3)  # D

        # 3
        glVertex3f(-0.2, 17.5, -8.3)  # A
        glVertex3f(0.2, 17.5, -8.3)  # B
        glVertex3f(0.2, 17.8, -8.3)  # C
        glVertex3f(-0.2, 17.8, -8.3)  # D

        glEnd()
        glPopMatrix()

    def stop(self):
        sys.exit()

    def update(self, value):
        # self._angle += 2.0
        # if self._angle > 360.0:
        #     self._angle -= 360
        glutPostRedisplay()
        glutTimerFunc(25, self.update, 0)
