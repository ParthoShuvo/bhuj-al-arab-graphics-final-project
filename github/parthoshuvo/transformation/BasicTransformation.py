from math import *
import numpy


def translate(given_coords, translate_coord):
    a = numpy.identity(4)
    for i in range(len(translate_coord)):
        a[i, 3] = translate_coord[i]
    given_coords.append(1.0)
    b = numpy.array(given_coords)[numpy.newaxis].T
    output_coords = numpy.matmul(a, b).ravel()
    output_coords = list(output_coords)
    output_coords.pop()
    return output_coords

def scale(given_coords, scaling_coord):
    output_coords = []
    if len(given_coords) == len(scaling_coord):
        for i in range(len(given_coords)):
            output_coords.append(given_coords[i] * scaling_coord[i])
    return output_coords


def rotate(given_coords, rotating_coord, angle):
    output_coords = given_coords
    if len(given_coords) == len(rotating_coord):
        for i in range(len(rotating_coord)):
            if rotating_coord[i] == 1.0:
                if i == 0:
                    output_coords = rotate_around_x(output_coords, angle)
                elif i == 1:
                    output_coords = rotate_around_y(output_coords, angle)
                else:
                    output_coords = rotate_around_z(output_coords, angle)
    return output_coords


def rotate_around_x(given_coords, angle):
    output_coords = []
    angle_in_radians = radians(angle)
    output_coords.append(given_coords[0])
    output_coords.append(given_coords[1]*cos(angle_in_radians) - given_coords[2]*sin(angle_in_radians))
    output_coords.append(given_coords[1]*sin(angle_in_radians) + given_coords[2]*cos(angle_in_radians))
    return output_coords

def rotate_around_y(given_coords, angle):
    output_coords = []
    angle_in_radians = radians(angle)
    output_coords.append(given_coords[0] * cos(angle_in_radians) + given_coords[2] * sin(angle_in_radians))
    output_coords.append(given_coords[1])
    output_coords.append(given_coords[0] * -sin(angle_in_radians) + given_coords[2] * cos(angle_in_radians))
    return output_coords

def rotate_around_z(given_coords, angle):
    output_coords = []
    angle_in_radians = radians(angle)
    output_coords.append(given_coords[0] * cos(angle_in_radians) - given_coords[1] * sin(angle_in_radians))
    output_coords.append(given_coords[0]*sin(angle_in_radians) + given_coords[1]*cos(angle_in_radians))
    output_coords.append(given_coords[2])
    return output_coords