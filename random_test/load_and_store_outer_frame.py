from github.parthoshuvo.transformation import BasicTransformation
from github.parthoshuvo.store_keeper import StoreKeeper

default_verticesZ = -2.0
default_verticesXY = [
    9.73, 0.71,
    9.8, 1.1,
    9.85, 1.3,
    9.9, 1.6,
    9.94, 1.8,
    10.017, 2.2,
    10.1, 2.8,
    10.14, 3.1,
    10.2, 3.6,
    10.25, 4.0,
    10.29, 4.6,
    10.3, 4.7,
    10.315, 4.8,
    10.336, 5.2,
    10.35, 5.6,
    10.355, 5.7,
    10.36, 6.7,
    10.35, 7.2,
    10.34, 7.3,
    10.315, 7.9,
    10.3, 8.0,
    10.285, 8.1,
    10.25, 8.6,
    10.2, 9.1,
    10.14, 9.5,
    10.1, 9.8,
    10.049, 10.1,
    10, 10.4,
    9.885, 10.9,
    9.8, 11.3,
    9.722, 11.7,
    9.6, 12.1,
    9.54, 12.3,
    9.4, 12.8,
    9.3, 13.1,
    9.2, 13.4,
    9.1, 13.7,
    8.9, 14.2,
    8.7, 14.7,
    8.53, 15.1,
    8.45, 15.3,
    8.4, 15.4,
    8.3, 15.6,
    8.2, 15.8,
    8.105, 16.0,
    8.013, 16.2,
    7.9, 16.4,
    7.85, 16.5,
    7.8, 16.6,
    7.684, 16.8,
    7.575, 17.0,
    7.425, 17.3,
    7.28, 17.5,
    7.12, 17.8,
    6.85, 18.2,
    6.78, 18.3,
    6.664, 18.5,
    6.58, 18.6,
    6.45, 18.8,
    6.31, 19.0,
    6.1, 19.3,
    5.945, 19.5,
    5.8, 19.7,
    5.64, 19.9,
    5.4, 20.2,
    5.15, 20.5,
    4.9, 20.8,
    4.8, 20.9,
    4.63, 21.1,
    4.45, 21.3,
    4.26, 21.5,
    4.06, 21.7,
    3.87, 21.9,
    3.66, 22.1,
    3.46, 22.3,
    3.32, 22.45
]

default_curvedBarWidth = 0.5
default_curvedBarFrontZ = -2.0
default_curvedBarBackZ = -2.25

translate_factor_x = -33.0
translate_factor_y = -12.0
translate_factor_z = -3.5
scale_factor_x = 0.2
scale_factor_y = 0.2
scale_factor_z = 0.2
rotate_y_factor = 1.0
rotate_x_factor = 0.0
rotate_z_factor = 0.0
angle = -80.0

# Front Side of the Curved-Bar
# glPushMatrix()
# glColor3f(0.5, 0.5, 0.5)
# glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
# glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
# glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
# glRotatef(-30.0, rotate_x_factor, 1, rotate_z_factor)
# glBegin(GL_TRIANGLE_FAN)

front_side_curved_bar = []
for i in range(0, 75 * 2, 2):
    coords = []
    sub_coords = [default_verticesXY[i], default_verticesXY[i + 1], default_curvedBarFrontZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i] + default_curvedBarWidth, default_verticesXY[i + 1], default_curvedBarFrontZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2] + default_curvedBarWidth, default_verticesXY[i + 3],
                  default_curvedBarFrontZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2], default_verticesXY[i + 3], default_curvedBarFrontZ]
    coords.append(sub_coords)
    front_side_curved_bar.append(coords)

# glEnd()
# glPopMatrix()

print("###FRONT_SIDE_CURVED_BAR", front_side_curved_bar)

# Back Side of the Curved-Bar
# glPushMatrix()
# glColor3f(0.5, 0.5, 0.5)
# glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
# glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
# glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
# glRotatef(-30.0, rotate_x_factor, 1, rotate_z_factor)
# glBegin(GL_TRIANGLE_FAN)
back_side_curved_bar = []
for i in range(0, 75 * 2, 2):
    coords = []
    sub_coords = [default_verticesXY[i], default_verticesXY[i + 1], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i] + default_curvedBarWidth, default_verticesXY[i + 1], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2] + default_curvedBarWidth, default_verticesXY[i + 3], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2], default_verticesXY[i + 3], default_curvedBarBackZ]
    coords.append(sub_coords)
    back_side_curved_bar.append(coords)

# glEnd()
# glPopMatrix()
print("###BACK_SIDE_CURVED_BAR", back_side_curved_bar)


# Right Side of the Curved-Bar
# glPushMatrix()
# glColor3f(0.5, 0.5, 0.5)
# glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
# glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
# glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
# glRotatef(-30.0, rotate_x_factor, 1, rotate_z_factor)
# glBegin(GL_TRIANGLE_FAN)
right_side_curved_bar = []
for i in range(0, 75 * 2, 2):
    coords = []
    sub_coords = [default_verticesXY[i] + default_curvedBarWidth, default_verticesXY[i + 1], default_curvedBarFrontZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i] + default_curvedBarWidth, default_verticesXY[i + 1], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2] + default_curvedBarWidth, default_verticesXY[i + 3], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2] + default_curvedBarWidth, default_verticesXY[i + 3],
                  default_curvedBarFrontZ]
    coords.append(sub_coords)
    right_side_curved_bar.append(coords)
# glEnd()
# glPopMatrix()
print("###RIGHT_SIDE_CURVED_BAR", right_side_curved_bar)

# Left Side of the Curved-Bar
# glPushMatrix()
# glColor3f(0.5, 0.5, 0.5)
# glRotatef(angle, rotate_x_factor, rotate_y_factor, rotate_z_factor)
# glScalef(scale_factor_x, scale_factor_y, scale_factor_z)
# glTranslatef(translate_factor_x, translate_factor_y, translate_factor_z)
# glRotatef(-30.0, rotate_x_factor, 1, rotate_z_factor)
# glBegin(GL_TRIANGLE_FAN)
left_side_curved_bar = []
for i in range(0, 75 * 2, 2):
    coords = []
    sub_coords = [default_verticesXY[i], default_verticesXY[i + 1], default_curvedBarFrontZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i], default_verticesXY[i + 1], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2], default_verticesXY[i + 3], default_curvedBarBackZ]
    coords.append(sub_coords)
    sub_coords = [default_verticesXY[i + 2], default_verticesXY[i + 3], default_curvedBarFrontZ]
    coords.append(sub_coords)
    left_side_curved_bar.append(coords)
# glEnd()
# glPopMatrix()
print("###LEFT_SIDE_CURVED_BAR", left_side_curved_bar)

outer_frame = {'front_side_curved_bar': front_side_curved_bar, 'back_side_curved_bar': back_side_curved_bar,
               'right_side_curved_bar': right_side_curved_bar, 'left_side_curved_bar': left_side_curved_bar}
StoreKeeper.store_object(outer_frame, 'outer_frame_objects')
outer_frame = StoreKeeper.open_object('outer_frame_objects')
