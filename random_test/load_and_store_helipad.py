from github.parthoshuvo.transformation import BasicTransformation
from github.parthoshuvo.store_keeper import StoreKeeper
from math import radians, cos, sin

center_x = -0.12
center_y = -5.0
radius = 0.3

helipad_coords = []
tex_coords = []

for i in range(360):
    theta = radians(i)
    x, z = center_x + radius * cos(theta), center_y + radius * sin(theta)
    coords = [x, z]
    helipad_coords.append(coords)
    tex_x, tex_y = 0.5 + 0.5 * cos(theta), 0.5 + 0.5 * sin(theta)
    coords = [tex_x, tex_y]
    tex_coords.append(coords)

print('####LENGTH#####', len(helipad_coords), len(tex_coords))
print('###HELIPAD coords###', helipad_coords)
print('###TEX COORDS#####', tex_coords)

helipad_objects = {'helipad_coords': helipad_coords, 'tex_coords': tex_coords}
StoreKeeper.store_object(helipad_objects, 'helipad_objects')
helipad_objects = StoreKeeper.open_object('helipad_objects')
print(helipad_objects)
