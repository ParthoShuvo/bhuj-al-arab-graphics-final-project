import shelve
import os
from math import *
import path


def round_360_degree():
    coord_set1 = []
    coord_set2 = []
    coord_set3 = []
    coord_set4 = []
    round_coord = []
    center_x = 0.0
    center_y = -6.0
    radius = 10.0
    for i in range(360):
        theta = radians(i)
        x, y, z = center_x + radius * cos(theta), -1.0, center_y + radius * sin(theta)
        if i in range(0, 90):
            coord_set1.append([x, y, z])
        elif i in range(90, 180):
            coord_set2.append([x, y, z])
        elif i in range(180, 270):
            coord_set3.append([x, y, z])
        elif i in range(270, 360):
            coord_set4.append([x, y, z])
        # print(x, y, z)

    round_coord.extend(coord_set3)
    round_coord.extend(coord_set4)
    round_coord.extend(coord_set1)
    round_coord.extend(coord_set2)
    return round_coord

def store_object(object, path, object_name):
    s = shelve.open(os.path.join(path, object_name), 'c')
    s[object_name] = object
    s.close()

def open_object(path, object_name):
    s = shelve.open(os.path.join(path, object_name), 'r')
    object = s[object_name]
    s.close()
    return object

round_coord = round_360_degree()
path = os.path.join(os.path.join(path.__root__, 'saved_files'), 'objects')

print(path)

store_object(round_coord, path, 'round_360_coord')

round_coord = open_object(path, 'round_360_coord')
print(len(round_coord))