from github.parthoshuvo.transformation import BasicTransformation
from github.parthoshuvo.store_keeper import StoreKeeper

vertices = [
    -0.1, 3.3, 0.0,
    -0.6, 3.0, 0.0,
    -0.7, 2.9, 0.0,
    -1.0, 2.4, 0.0,
    -1.1, 2.2, 0.0,
    -1.2, 2.0, 0.0,
    -1.4, 1.7, 0.0,
    -1.5, 1.5, 0.0,
    -1.6, 1.4, 0.0,
    -1.7, 1.2, 0.0,
    -1.9, 0.9, 0.0,
    -2.0, 0.7, 0.0,
    -2.2, 0.4, 0.0,
    -2.3, 0.2, 0.0,
    -2.5, -0.1, 0.0,
    -2.6, -0.3, 0.0,
    -2.8, -0.6, 0.0,
    -2.9, -0.8, 0.0,
    -3.0, -1.3, 0.0,
    -3.0, -1.4, 0.0,
    -2.9, -1.7, 0.0,
    -2.8, -1.9, 0.0,
    -2.7, -2.0, 0.0,
    -2.3, -2.4, 0.0,
    -2.2, -2.5, 0.0,
    -2.0, -2.6, 0.0,
    -1.5, -2.8, 0.0,
    -1.4, -2.83, 0.0,
    -1.3, -2.9, 0.0,
    -0.57, -2.97, 0.0,  # Was: y - 2.9
    -0.5, -3.0, 0.0,
    -0.4, -3.0, 0.0,
    -0.3, -3.0, 0.0,
    -0.2, -3.0, 0.0,
    -0.1, -3.0, 0.0,
    0.0, -3.0, 0.0,
    0.1, -3.0, 0.0,
    0.2, -3.0, 0.0,
    0.3, -3.0, 0.0,
    0.4, -3.0, 0.0,
    0.5, -3.0, 0.0,
    0.6, -3.0, 0.0,
    0.7, -3.0, 0.0,
    1.4, -2.9, 0.0,
    1.9, -2.8, 0.0,
    2.2, -2.72, 0.0,
    2.5, -2.6, 0.0,
    2.8, -2.4, 0.0,
    2.9, -2.33, 0.0,
    3.2, -2.15, 0.0,
    3.5, -1.5, 0.0,
    3.5, -0.8, 0.0,
    3.3, -0.39, 0.0,
    3.4, -0.6, 0.0,
    3.1, -0.1, 0.0,
    2.9, 0.2, 0.0,
    2.7, 0.5, 0.0,
    2.5, 0.8, 0.0,
    2.3, 1.1, 0.0,
    2.1, 1.4, 0.0,
    2.0, 1.5, 0.0,
    1.9, 1.7, 0.0,
    1.7, 2.0, 0.0,
    1.5, 2.3, 0.0,
    1.2, 2.72, 0.0,
    1.1, 2.9, 0.0,
    1.0, 3.0, 0.0,
    0.9, 3.1, 0.0,
    0.8, 3.2, 0.0,  # was: x: 0.3
    0.5, 3.3, 0.0,
    0.0, 3.3, 0.0,
]

y = -2.5
z = -5.0
#

# glPushMatrix()
# glTranslatef(0.0, y, z)

# glRotatef(self._angle, 0, 1, 0)
# glRotatef(90.0, 1, 0, 0)

# Drawing the upper face

# glColor3f(0.01, 0.05, 0.03)

# glBegin(GL_POLYGON)

upper_face = []
for i in range(0, 71 * 3, 3):
    coords = [vertices[i] / 3, vertices[i + 1] / 3, -0.2]
    # translate_coord = [0.0, -2.5, -5.0]
    # coords = BasicTransformation.translate(coords, translate_coord)
    # rotating_coord = [0.0, 1.0, 0.0]
    # coords = BasicTransformation.rotate(coords, rotating_coord, 180.0)
    # rotating_coord = [1.0, 0.0, 0.0]
    # coords = BasicTransformation.rotate(coords, rotating_coord, 90.0)
    upper_face.append(coords)

print('###UPPER FACE###', upper_face)

# Drawing dark-radish plane
# glPushMatrix()
# glColor3f(0.35, 0.05, 0.1)
# glTranslatef(0.0, y, z)
# glRotatef(self._angle, 0, 1, 0)
# glRotatef(90.0, 1, 0, 0)
# glScalef(0.95, 0.95, 1.01)
#

# glBegin(GL_POLYGON)
dark_radish_face = []
for i in range(0, 71 * 3, 3):
    coords = [vertices[i] / 3, vertices[i + 1] / 3, -0.2]
    # translate_coord = [0.0, y, z]
    # coords = BasicTransformation.translate(coords, translate_coord)
    # # rotating_coord = [0.0, 1.0, 0.0]
    # # coords = BasicTransformation.rotate(coords, rotating_coord, 180.0)
    # rotating_coord = [1.0, 0.0, 0.0]
    # coords = BasicTransformation.rotate(coords, rotating_coord, 90.0)
    # scaling_coord = [0.95, 0.95, 1.01]
    # coords = BasicTransformation.scale(coords, scaling_coord)
    dark_radish_face.append(coords)

# glEnd()
# glFlush()
# glPopMatrix()



print('###DARK_REDDISH_FACE###', dark_radish_face)

# Drawing Greenish plane
# glPushMatrix()
# glColor3f(0.0, 0.3, 0.1)
# glTranslatef(0.0, y, z)
# glRotatef(self._angle, 0, 1, 0)
# glRotatef(90.0, 1, 0, 0)
# glScalef(0.85, 0.85, 1.02)

dark_greenish_face = []
for i in range(0, 71 * 3, 3):
    coords = [vertices[i] / 3, vertices[i + 1] / 3, -0.2]
    # translate_coord = [0.0, y, z]
    # coords = BasicTransformation.translate(coords, translate_coord)
    # # rotating_coord = [0.0, 1.0, 0.0]
    # # coords = BasicTransformation.rotate(coords, rotating_coord, 180.0)
    # rotating_coord = [1.0, 0.0, 0.0]
    # coords = BasicTransformation.rotate(coords, rotating_coord, 90.0)
    # scaling_coord = [0.95, 0.95, 1.01]
    # coords = BasicTransformation.scale(coords, scaling_coord)
    dark_greenish_face.append(coords)

print('###DARK_GREENISH_FACE###', dark_greenish_face)

# Drawing the lower face
# glPushMatrix()
# glColor3f(0.3, 0.7, 0.7)
# glTranslatef(0.0, y, z)
# glRotatef(self._angle, 0, 1, 0)
# glRotatef(90.0, 1, 0, 0)
#
# glBegin(GL_POLYGON)
lower_face = []
for i in range(0, 71 * 3, 3):
    coords = [vertices[i] / 3, vertices[i + 1] / 3, -0.2]
    # translate_coord = [0.0, y, z]
    # coords = BasicTransformation.translate(coords, translate_coord)
    # # rotating_coord = [0.0, 1.0, 0.0]
    # # coords = BasicTransformation.rotate(coords, rotating_coord, 180.0)
    # rotating_coord = [1.0, 0.0, 0.0]
    # coords = BasicTransformation.rotate(coords, rotating_coord, 90.0)
    # scaling_coord = [0.95, 0.95, 1.01]
    # coords = BasicTransformation.scale(coords, scaling_coord)
    lower_face.append(coords)
# glEnd()
# glFlush()
# glPopMatrix()
print('###LOWER_FACE###', lower_face)

# Drawing the side-face
# glPushMatrix()
# glColor3f(0.0, 0.0, 0.7)
# glTranslatef(0.0, y, z)
# glRotatef(self._angle, 0, 1, 0)
# glRotatef(90.0, 1, 0, 0)
# glBegin(GL_POLYGON)

side_face = []
for i in range(0, 70 * 3, 3):
    coords = [vertices[i] / 3, vertices[i + 1] / 3, -0.2]
    side_face.append(coords)
    coords = [vertices[i] / 3, vertices[i + 1] / 3, 0.2]
    side_face.append(coords)
    coords = [vertices[i + 3] / 3, vertices[i + 4] / 3, 0.2]
    side_face.append(coords)
    coords = [vertices[i + 3] / 3, vertices[i + 4] / 3, -0.2]
    side_face.append(coords)
# glFlush()
# glPopMatrix()
print('###SIDE_FACE###', side_face)

platform_objects = {'upper_face': upper_face, 'dark_reddish_face': dark_radish_face,
                    'dark_greenish_face': dark_greenish_face, 'lower_face': lower_face,
                    'side_face': side_face}
StoreKeeper.store_object(platform_objects, 'platform_objects')
platform_objects = StoreKeeper.open_object('platform_objects')
